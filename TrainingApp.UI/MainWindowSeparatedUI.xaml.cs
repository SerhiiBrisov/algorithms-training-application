﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using TrainingApp.ViewModel.Services;
using TrainingApp.ViewModel.ViewModels;

namespace TrainingApp.UI
{
    /// <summary>
    /// Interaction logic for MainWindowSeparatedUI.xaml.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public partial class MainWindowSeparatedUI : Window
    {
        /// <summary>
        /// Method that clear DrawingCanvas and stack panel for labels OutputTextBox
        /// and puts new elements into them.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private void Draw()
        {
            this.DrawingCanvas.Children.Clear();
            this.OutputTextBox.Children.Clear();
            ((ApplicationViewModel)DataContext).ResizeAndDrawCommand.Execute(null);
            foreach (var item in ((ApplicationViewModel)DataContext).Lines)
            {
                this.DrawingCanvas.Width = ((ApplicationViewModel)DataContext).WindowWidth - DrawingScroll.Margin.Left * 2;
                this.DrawingCanvas.Children.Add(item);

            }
            if (((ApplicationViewModel)DataContext).SortingTime.Length != 0)
            {
                this.DrawingCanvas.Children.Add(
                new Label
                {
                    FontStyle = FontStyles.Italic,
                    Foreground = Brushes.Blue,
                    FontSize = (((ApplicationViewModel)DataContext).WindowWidth + ((ApplicationViewModel)DataContext).WindowHeight) / 60,
                    VerticalAlignment = VerticalAlignment.Top,
                    HorizontalAlignment = HorizontalAlignment.Right,
                    Content = ((ApplicationViewModel)DataContext).SortingTime
                });
            }
            foreach (var item in ((ApplicationViewModel)this.DataContext).Labels)
            {
                this.OutputTextBox.Width = ((ApplicationViewModel)this.DataContext).WindowWidth - this.StackScroll.Margin.Left * 2 - this.RunButton.Width;
                this.OutputTextBox.Children.Add(item);
            }
        }

        /// <summary>
        /// Creates window component and the view model and assings to DataContext of this window.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public MainWindowSeparatedUI()
        {
            this.DataContext = new ApplicationViewModel(new DefaultIntGeneratingService(), new SortingService<int>(), new DrawingService());
            this.InitializeComponent();
        }

        /// <summary>
        /// Apply Settings handler applies settings properties to <see cref="ApplicationViewModel"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private void OnApplySettings(object sender, EventArgs e)
        {
            var settings = (SettingsViewModel)(sender as SettingsWindow).DataContext;
            ((ApplicationViewModel)this.DataContext).Delay = settings.Delay;
            ((ApplicationViewModel)this.DataContext).GradientColor = settings.GradientColor;
            ((ApplicationViewModel)this.DataContext).Quantity = settings.Quantity;
            ((ApplicationViewModel)this.DataContext).SortingType = settings.SortingType;
            this.IsEnabled = true;
        }

        /// <summary>
        /// Prints asynchronously labels with digits and lines on UI.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private void PrintLabelsAndLines(object sender, RoutedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Render, (ThreadStart)delegate ()
            {
                this.Draw();
            });
        }

        /// <summary>
        /// SettingsButton click event handler initializes settings window..
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            var settings = new SettingsWindow((ApplicationViewModel)this.DataContext);
            settings.Show();
            this.IsEnabled = false;
            settings.Closed += OnApplySettings;
        }

        /// <summary>
        /// Size changed event handler for Main Window.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private void Window_SizeChanged(object sender, SizeChangedEventArgs e) => this.Draw();
    }
}