﻿using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using TrainingApp.ViewModel.Services;
using TrainingApp.ViewModel.ViewModels;

namespace TrainingApp.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Value changed event handler for Delay slider.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private void DelaySlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider slider = sender as Slider;
            ((ApplicationViewModel)DataContext).Delay = slider.Value;
        }

        /// <summary>
        /// Method that clear DrawingCanvas and stack panel for labels OutputTextBox
        /// and puts new elements into them.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private void Draw()
        {
            this.DrawingCanvas.Children.Clear();
            this.OutputTextBox.Children.Clear();
            ((ApplicationViewModel)DataContext).ResizeAndDrawCommand.Execute(null);
            foreach (var item in ((ApplicationViewModel)DataContext).Lines)
            {
                this.DrawingCanvas.Width = ((ApplicationViewModel)DataContext).WindowWidth - DrawingScroll.Margin.Left * 4;
                this.DrawingCanvas.Children.Add(item);
            }
            foreach (var item in ((ApplicationViewModel)DataContext).Labels)
            {
                this.OutputTextBox.Width = ((ApplicationViewModel)DataContext).WindowWidth - StackScroll.Margin.Left * 4;
                this.OutputTextBox.Children.Add(item);
            }
        }

        /// <summary>
        /// Creates window component and the view model and assings to DataContext of this window.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public MainWindow()
        {
            this.DataContext = new ApplicationViewModel(new DefaultIntGeneratingService(), new SortingService<int>(), new DrawingService());
            this.InitializeComponent();
        }

        /// <summary>
        /// Prints asynchronously labels with digits and lines on UI.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private void PrintLabelsAndLines(object sender, RoutedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Render, (ThreadStart)delegate()
            {
                this.Draw();
            });
        }

        /// <summary>
        /// Value changed event handler for Quantity slider.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private void QuantitySlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider slider = sender as Slider;
            ((ApplicationViewModel)DataContext).Quantity = slider.Value;
        }

        /// <summary>
        /// Size changed event handler for Main Window.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private void Window_SizeChanged(object sender, SizeChangedEventArgs e) => this.Draw();
    }
}