﻿using System.Windows;
using System.Windows.Controls;
using TrainingApp.ViewModel.ViewModels;

namespace TrainingApp.UI
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public partial class SettingsWindow : Window
    {
        /// <summary>
        /// Click event handler for Apply button.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private void ApplyButton_Click(object sender, RoutedEventArgs e) => this.Close();

        /// <summary>
        /// Value changed event handler for Delay slider.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private void DelaySlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider slider = sender as Slider;
            ((SettingsViewModel)this.DataContext).Delay = slider.Value;
        }

        /// <summary>
        /// Creates window component and the view model and assings to DataContext of this window.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public SettingsWindow(ApplicationViewModel applicationViewModel)
        {
            this.DataContext = new SettingsViewModel(
                applicationViewModel.Quantity,
                applicationViewModel.Delay,
                applicationViewModel.GradientColor,
                applicationViewModel.SortingType
                );
            this.InitializeComponent();
        }

        /// <summary>
        /// Value changed event handler for Quantity slider.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private void QuantitySlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider slider = sender as Slider;
            ((SettingsViewModel)this.DataContext).Quantity = slider.Value;
        }
    }
}