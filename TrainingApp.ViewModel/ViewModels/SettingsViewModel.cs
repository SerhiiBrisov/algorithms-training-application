﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using TrainingApp.ViewModel.Enums;

namespace TrainingApp.ViewModel.ViewModels
{
    /// <summary>
    /// View model class used for the MainWindowSeparatedUI View.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class SettingsViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Holds the value for <see cref="Delay"/> property.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private double delay;

        /// <summary>
        /// Holds the value for <see cref="GradientColor"/> property.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private GradientColor gradientColor;

        /// <summary>
        /// Holds the value for <see cref="Quantity"/> property.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private double quantity;

        /// <summary>
        /// Holds the value for <see cref="SortingType"/> property.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private SortingType sortingType;

        /// <summary>
        /// Gets and sets delay after every iteration in sorting algorithm
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The delay after every iteration in sorting algorithm.</value>
        public double Delay
        {
            get => this.delay;
            set
            {
                this.delay = value;
                this.OnPropertyChanged(nameof(this.Delay));
            }
        }

        /// <summary>
        /// Gets and sets the choosen color of lines
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The choosen color of lines.</value>
        public GradientColor GradientColor
        {
            get => this.gradientColor;
            set
            {
                this.gradientColor = value;
                this.OnPropertyChanged(nameof(this.GradientColor));
            }
        }

        /// <summary>
        /// Gets and sets quantity of elements
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The quantity of elements.</value>
        public double Quantity
        {
            get => this.quantity;
            set
            {
                this.quantity = value;
                this.OnPropertyChanged(nameof(this.Quantity));
            }
        }

        /// <summary>
        /// The even of the <see cref="SettingsViewModel"/> property changes.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
		/// The handler of the <see cref="SettingsViewModel"/> changes.
		/// </summary>
		/// <owner>Serhii Brysov</owner>
		/// <param name="prop">The name of property.</param>
        public void OnPropertyChanged([CallerMemberName] string prop = "") =>
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        /// <summary>
        /// Gets and sets the choosen type of sorting
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The choosen type of sorting.</value>
        public SortingType SortingType
        {
            get => this.sortingType;
            set
            {
                this.sortingType = value;
                this.OnPropertyChanged(nameof(this.SortingType));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsViewModel"/> class.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public SettingsViewModel(double quantity, double delay, GradientColor gradientColor, SortingType sortingType)
        {
            this.Quantity = quantity;
            this.Delay = delay;
            this.GradientColor = gradientColor;
            this.SortingType = sortingType;
        }
    }
}