﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using System.Windows.Shapes;
using TrainingApp.Business.Models;
using TrainingApp.ViewModel.Commands;
using TrainingApp.ViewModel.Enums;
using TrainingApp.ViewModel.ViewModelInterfaces;

namespace TrainingApp.ViewModel.ViewModels
{
    /// <summary>
    /// View model class used for the Application View.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class ApplicationViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Holds the value for <see cref="ComparableModel"/> property.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private ComparableModel<int> comparableModel;

        /// <summary>
        /// Holds the value for <see cref="Delay"/> property.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private double delay;

        /// <summary>
        /// Holds the Drawing service instance.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private readonly IDrawingService drawingService;

        /// <summary>
        /// Command used for generation of model.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private RelayCommand generateCommand;

        /// <summary>
        /// Holds the Generation service instance.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private readonly IGenerationService<int> generationService;

        /// <summary>
        /// Holds the value for <see cref="GradientColor"/> property.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private GradientColor gradientColor;

        /// <summary>
        /// Holds the value for <see cref="Labels"/> property.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private Label[] labels;

        /// <summary>
        /// Holds the value for <see cref="Lines"/> property.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private Line[] lines;

        /// <summary>
        /// Holds the value for <see cref="Quantity"/> property.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private double quantity;

        /// <summary>
        /// Command used for draw elements after resize window.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private RelayCommand resizeAndDrawCommand;

        /// <summary>
        /// Command used for sorting the elements in model.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private RelayCommand sortingCommand;

        /// <summary>
        /// Holds the Sorting service instance.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private readonly ISortingService<int> sortingService;

        /// <summary>
        /// Holds the value for <see cref="SortingType"/> property.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private SortingType sortingType;

        /// <summary>
        /// Holds the value for <see cref="SortingTime"/> property.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private string sortingTime;

        /// <summary>
        /// Holds the value for <see cref="WindowHeight"/> property.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private double windowHeight;

        /// <summary>
        /// Holds the value for <see cref="WindowWidth"/> property.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private double windowWidth;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationViewModel"/> class.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="generationService">The realization of <see cref="IGenerationService{int}"/>.</param>
        /// <param name="sortingService">The realization of <see cref="ISortingService{int}"/>.</param>
        /// <param name="drawingService">The realization of <see cref="IDrawingService"/>.</param>
        public ApplicationViewModel(IGenerationService<int> generationService, ISortingService<int> sortingService, IDrawingService drawingService)
        {
            this.generationService = generationService;
            this.sortingService = sortingService;
            this.drawingService = drawingService;
            this.comparableModel = new ComparableModel<int>();
            this.Quantity = comparableModel.DefaultLength;
            this.Delay = 10;
            this.GenerateCommand.Execute(null);
            this.SortingTime = string.Empty;
        }

        /// <summary>
        /// Gets and sets data to model.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The model.</value>
        public ComparableModel<int> ComparableModel
        {
            get => this.comparableModel;
            set
            {
                this.comparableModel = value;
                this.OnPropertyChanged(nameof(this.ComparableModel));
            }
        }

        /// <summary>
        /// Gets and sets delay after every iteration in sorting algorithm.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The delay after every iteration in sorting algorithm.</value>
        public double Delay
        {
            get => this.delay;
            set
            {
                this.delay = value;
                this.OnPropertyChanged(nameof(this.Delay));

            }
        }

        /// <summary>
        /// Command used for generation of model.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public RelayCommand GenerateCommand
        {
            get
            {
                return generateCommand ??= new RelayCommand(obj =>
                {
                    this.comparableModel = new ComparableModel<int>(generationService.Generate((int)this.Quantity));
#pragma warning disable S2551 // Shared resources should not be used for locking
                    lock (this)
#pragma warning restore S2551 // Shared resources should not be used for locking
                    {
                        this.ResizeAndDrawCommand.Execute(null);
                    }
                });
            }
        }

        /// <summary>
        /// Gets and sets the choosen color of lines.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The choosen color of lines.</value>
        public GradientColor GradientColor
        {
            get => this.gradientColor;
            set
            {
                this.gradientColor = value;
                this.OnPropertyChanged(nameof(this.GradientColor));
            }
        }

        /// <summary>
        /// Gets and sets array of labels with numbers.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The labels with numbers.</value>
        public Label[] Labels
        {
            get => this.labels;
            set
            {
                this.labels = value;
                this.OnPropertyChanged(nameof(this.Lines));
            }
        }

        /// <summary>
        /// Gets and sets array of lines.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The array of lines.</value>
        public Line[] Lines
        {
            get => this.lines;
            set
            {
                this.lines = value;
                this.OnPropertyChanged(nameof(this.Lines));
            }
        }

        /// <summary>
        /// The handler of the <see cref="ApplicationViewModel"/> changes.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="prop">The name of property.</param>
        public void OnPropertyChanged([CallerMemberName] string prop = "") =>
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        /// <summary>
        /// The even of the <see cref="ApplicationViewModel"/> property changes.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets and sets quantity of elements
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The quantity of elements.</value>
        public double Quantity
        {
            get => this.quantity;
            set
            {
                this.quantity = value;
                this.OnPropertyChanged(nameof(this.Quantity));
            }
        }

        /// <summary>
        /// Command used for draw elements after resize window.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public RelayCommand ResizeAndDrawCommand
        {
            get
            {
                return resizeAndDrawCommand ??= new RelayCommand(obj =>
                {
                    Lines = drawingService.InitializeLines(comparableModel.SortingCollection, gradientColor, WindowWidth, WindowHeight);
                    Labels = drawingService.InitializeLabels(comparableModel.SortingCollection, WindowWidth, WindowHeight);
                });
            }
        }

        /// <summary>
        /// Command used for sorting the elements in model.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public RelayCommand SortingCommand
        {
            get
            {
                return sortingCommand ??= new RelayCommand(obj =>
                {
                    sortingService.SortingType = this.sortingType;
                    Stopwatch stopWatch = new Stopwatch();
                    stopWatch.Start();
                    try
                    {
                        sortingService.Sort<int>(comparableModel, Comparer<int>.Default, (int)Delay);
                    }
                    finally
                    {
                        sortingService?.Dispose();
                    }
                    stopWatch.Stop();
#pragma warning disable S2551 // Shared resources should not be used for locking
                    lock (this)
#pragma warning restore S2551 // Shared resources should not be used for locking
                    {
                        this.ResizeAndDrawCommand.Execute(null);
                    }
                    TimeSpan ts = stopWatch.Elapsed;
                    SortingTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                });
            }
        }

        /// <summary>
        /// Gets and sets sorting time after execution of sorting algorithm
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The delay after every iteration in sorting algorithm.</value>
        public string SortingTime
        {
            get => this.sortingTime;
            set
            {
                this.sortingTime = value;
                this.OnPropertyChanged(nameof(this.SortingTime));
            }
        }

        /// <summary>
        /// Gets and sets the choosen type of sorting
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The choosen type of sorting.</value>
        public SortingType SortingType
        {
            get => this.sortingType;
            set
            {
                this.sortingType = value;
                this.OnPropertyChanged(nameof(this.SortingType));
            }
        }

        /// <summary>
        /// Gets and sets Width of main window
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The Width of main window.</value>
        public double WindowWidth
        {
            get => this.windowWidth;
            set
            {
                this.windowWidth = value;
                this.OnPropertyChanged(nameof(this.WindowWidth));
            }
        }

        /// <summary>
        /// Gets and sets Height of main window
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The Height of main window.</value>
        public double WindowHeight
        {
            get => this.windowHeight;
            set
            {
                this.windowHeight = value;
                this.OnPropertyChanged(nameof(this.WindowHeight));
            }
        }
    }
}