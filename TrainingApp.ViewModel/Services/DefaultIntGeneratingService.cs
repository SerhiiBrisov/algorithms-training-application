﻿using System;
using System.Collections.Generic;
using TrainingApp.ViewModel.ViewModelInterfaces;

namespace TrainingApp.ViewModel.Services
{
    /// <summary>
    /// Class with realization of <see cref="IGenerationService{int}"/> for UI.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class DefaultIntGeneratingService : IGenerationService<int>
    {
        /// <summary>
        /// Dafault integer collection generation.
        /// </summary>
        /// <owner>Serhii Brysov</owner>  
        /// <returns>
        /// <see cref="IEnumerable{int}"/> collection with constant length.
        /// </returns>
        public IEnumerable<int> Generate()
        {
            Random random = new Random();
            const int length = 10;
            int[] numbers = new int[length];
            for (int i = 0; i < numbers.Length; i++)
                numbers[i] = random.Next(5, 175);            
            return numbers;
        }

        /// <summary>
        /// Generation of integer collection with adjusted quantity of elements.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="quantity">Number of elements that should be generated.</param>
        /// /// <returns>
        /// <see cref="IEnumerable{int}"/> collection with adjusted length.
        /// </returns>
        public IEnumerable<int> Generate(int quantity)
        {
            Random random = new Random();

            int[] numbers = new int[quantity];
            for (int i = 0; i < numbers.Length; i++)
                numbers[i] = random.Next(5, 175);            
            return numbers;
        }
    }
}