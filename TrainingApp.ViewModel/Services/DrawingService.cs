﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using TrainingApp.ViewModel.Enums;
using TrainingApp.ViewModel.ViewModelInterfaces;

namespace TrainingApp.ViewModel.Services
{
    /// <summary>
    /// Class with realization of <see cref="IDrawingService"/> for UI.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class DrawingService : IDrawingService
    {
        /// <summary>
        /// Generate labels with numbers in it according to input data.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="collection">Integer collection with angles of scope.</param>
        /// <param name="windowHeight">Height of Main Window.</param>
        /// <param name="windowWidth">Width of Main Window.</param
        public Label[] InitializeLabels(ICollection<int> collection, double windowWidth, double windowHeight)
        {
            Label[] labels = new Label[collection.Count];
            int iterator = 0;
            if (double.IsNaN(windowWidth) || double.IsNaN(windowHeight) || windowWidth == 0 || windowHeight == 0)
                windowWidth = 749;
            foreach (int item in collection)
            {
                Label label = new Label
                {
                    Background = Brushes.Wheat,
                    FontSize = windowWidth / collection.Count / 3,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Content = item,
                    BorderThickness = new Thickness(4)
                };
                labels[iterator] = label;
                iterator++;
            }
            return labels;
        }

        /// <summary>
        /// Generate lines according to input data.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="collection">Integer collection with angles of scope.</param>
        /// <param name="gradientColor">Color of lines.</param>
        /// <param name="windowHeight">Height of Main Window.</param>
        /// <param name="windowWidth">Width of Main Window.</param>         
        public Line[] InitializeLines(ICollection<int> collection, GradientColor gradientColor, double windowWidth, double windowHeight)
        {
            if (double.IsNaN(windowWidth) || double.IsNaN(windowHeight) || windowWidth == 0 || windowHeight == 0)
            {
                windowWidth = 749;
                windowHeight = 545;
            }
            double r = (windowWidth * windowHeight * (windowHeight / windowWidth)) * 0.000318;
            double a = (windowWidth * windowHeight * (windowHeight / windowWidth)) * 0.000343;
            double b = (windowWidth * windowHeight * (windowHeight / windowWidth)) * 0.000563;
            Line[] lines = new Line[collection.Count];
            int i = 0;
            foreach (var item in collection)
            {
                Line line = new Line
                {
                    X1 = a + (double)(i + 1) * 30,
                    Y1 = b
                };
                line.X2 = -Math.Cos(((double)item / 180) * Math.PI) * r + line.X1;
                line.Y2 = -Math.Sin(((double)item / 180) * Math.PI) * r + line.Y1;
                line.StrokeThickness = 3;
                SolidColorBrush brush = null;
                switch (gradientColor.ToString())
                {
                    case nameof(GradientColor.Red):
                        {
                            brush = new SolidColorBrush(Color.FromArgb(250, (byte)(item + 70), 0, 0));
                            break;
                        }
                    case nameof(GradientColor.Green):
                        {
                            brush = new SolidColorBrush(Color.FromArgb(250, 0, (byte)(item + 70), 0));
                            break;
                        }
                    case nameof(GradientColor.Blue):
                        {
                            brush = new SolidColorBrush(Color.FromArgb(250, 0, 0, (byte)(item + 70)));
                            break;
                        }
                    default:
                        break;
                }
                line.Stroke = brush;
                lines[i] = line;
                i++;
            }
            return lines;
        }

        /// <summary>
        /// Generate label according to input string.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="sortingTime">String representation of time.</param>
        /// <param name="windowHeight">Height of Main Window.</param>
        /// <param name="windowWidth">Width of Main Window.</param>
        public Label PrintSortingTime(string sortingTime, double windowWidth, double windowHeight)
        {
            Label time = new Label
            {
                Background = Brushes.Wheat,
                FontSize = windowWidth + windowHeight / 60,
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = HorizontalAlignment.Right,
                Content = sortingTime
            };
            return time;
        }
    }
}