﻿using System;
using System.Collections.Generic;
using TrainingApp.Business.Models;
using TrainingApp.Business.Sortings;
using TrainingApp.ViewModel.Enums;
using TrainingApp.ViewModel.ViewModelInterfaces;

namespace TrainingApp.ViewModel.Services
{
    /// <summary>
    /// Class with realization of <see cref="IGenerationService{T}"/> for sorting algorithms.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class SortingService<T> : ISortingService<T> where T : IComparable
    {
        /// <summary>
        /// Flag wich become true when list is disposed.
        /// <remarks>
        /// By default is false
        /// </remarks>
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private bool disposedValue = false;

        /// <summary>
        /// Gets or sets instance of <see cref="BubbleSort{T}"/> .
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public BubbleSort<T> BubbleSort { get; set; }

        /// <summary>
        /// Method that makes Garbage Collector finalize this item.
        /// </summary>
        /// <remarks>
        /// Realisation of IDisposable interface
        /// </remarks>
        /// <owner>Serhii Brysov</owner>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Method that clears memory from this object.
        /// </summary>
        /// <param name="disposing">
        /// If true - list is already disposed.
        /// If false - field with reference to list will be clear, and fields will take default values.
        /// </param>
        /// <owner>Serhii Brysov</owner>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue && disposing)
            {
                this.BubbleSort = null;
                this.InsertionSort = null;
                this.ShakerSort = null;
            }
            this.disposedValue = true;
        }

        /// <summary>
        /// Gets or sets instance of <see cref="InsertionSort{T}"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public InsertionSort<T> InsertionSort { get; set; }

        /// <summary>
        /// Gets or sets instance of <see cref="ShakerSort{T}"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public ShakerSort<T> ShakerSort { get; set; }

        /// <summary>
        /// Method that calls instances of choosen sorting type.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="collection">Model includes generic collection that will be sorted.</param>
        /// <param name="comparer">Custom comparer necessary to make collection sorted.</param>
        /// <param name="delay">Number of miliseconds of delay on every iteration.</param>
        public void Sort<IComparable>(ComparableModel<T> collection, IComparer<T> comparer, int delay)
        {
            switch (this.SortingType.ToString())
            {
                case nameof(SortingType.BubbleSort):
                    this.BubbleSort = new BubbleSort<T>();
                    this.BubbleSort.Sort(collection, comparer, delay);
                    this.disposedValue = false;
                    break;
                case nameof(SortingType.InsertionSort):
                    this.InsertionSort = new InsertionSort<T>();
                    this.InsertionSort.Sort(collection, comparer, delay);
                    this.disposedValue = false;
                    break;
                case nameof(SortingType.ShakerSort):
                    this.ShakerSort = new ShakerSort<T>();
                    this.ShakerSort.Sort(collection, comparer, delay);
                    this.disposedValue = false;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="SortingService{T}"/> class.
        /// </summary>
        ///  <owner>Serhii Brysov</owner>
        ~SortingService() => this.Dispose(false);

        /// <summary>
        /// Gets or sets type of sorting.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public SortingType SortingType { get; set; }
    }
}