﻿using System;
using System.Collections.Generic;
using TrainingApp.Business.Models;
using TrainingApp.ViewModel.Enums;

namespace TrainingApp.ViewModel.ViewModelInterfaces
{
    /// <summary>
    /// All classes that includes sorting service should implement this interface.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public interface ISortingService<T> : IDisposable where T : IComparable
    {
        /// <summary>
        /// Gets or sets type of sorting.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public SortingType SortingType { get; set; }

        /// <summary>
        /// Pattern of method of sorting.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="collection">Model includes generic collection that will be sorted.</param>
        /// <param name="comparer">Custom comparer necessary to make collection sorted.</param>
        /// <param name="delay">Number of miliseconds of delay on every iteration.</param>
        public void Sort<IComparable>(ComparableModel<T> collection, IComparer<T> comparer, int delay);
    }
}