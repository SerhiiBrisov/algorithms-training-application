﻿using System.Collections.Generic;

namespace TrainingApp.ViewModel.ViewModelInterfaces
{
    /// <summary>
    /// All classes that includes generating service should implement this interface.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public interface IGenerationService<out T>
    {
        /// <summary>
        /// Dafault collection generation.
        /// </summary>
        /// <owner>Serhii Brysov</owner>  
        IEnumerable<T> Generate();

        /// <summary>
        /// Generation of collection with adjusted quantity of elements.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="quantity">Number of elements that should be generated.</param>
        IEnumerable<T> Generate(int quantity);
    }
}