﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Shapes;
using TrainingApp.ViewModel.Enums;

namespace TrainingApp.ViewModel.ViewModelInterfaces
{
    /// <summary>
    /// Specification for drawing service that creates UI elements.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public interface IDrawingService
    {
        /// <summary>
        /// Generate labels according to input data.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="collection">Integer collection with angles of scope.</param>
        /// <param name="windowHeight">Height of Main Window.</param>
        /// <param name="windowWidth">Width of Main Window.</param>
        public Label[] InitializeLabels(ICollection<int> collection, double windowWidth, double windowHeight);

        /// <summary>
        /// Generate lines according to input data.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="collection">Integer collection with angles of scope.</param>
        /// <param name="gradientColor">Color of lines.</param>
        /// <param name="windowHeight">Height of Main Window.</param>
        /// <param name="windowWidth">Width of Main Window.</param>
        public Line[] InitializeLines(ICollection<int> collection, GradientColor gradientColor, double windowWidth, double windowHeight);
    }
}