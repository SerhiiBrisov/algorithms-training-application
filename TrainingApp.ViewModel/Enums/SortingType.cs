﻿namespace TrainingApp.ViewModel.Enums
{
    /// <summary>
    /// Enum to identify sorting algorithms type.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public enum SortingType
    {
        BubbleSort,
        InsertionSort,
        ShakerSort
    }
}