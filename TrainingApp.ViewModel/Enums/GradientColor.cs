﻿namespace TrainingApp.ViewModel.Enums
{
    /// <summary>
    /// Enum to identify color.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public enum GradientColor
    {
        Red,
        Green,
        Blue
    }
}