﻿using System;
using System.Windows.Input;

namespace TrainingApp.ViewModel.Commands
{
    /// <summary>
    /// Toolkit for MVVM pattern, implementation of ICommand.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class RelayCommand : ICommand
    {
        /// <summary>
        /// Action used for the Execute method that the ICommand interface requires. 
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private readonly Action<object> execute;

        /// <summary>
        /// It is a delegate for the CanExecute method that’s specified by ICommand. 
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private readonly Func<object, bool> canExecute;

        /// <summary>
        /// Event handler for event that called if <see cref="canExecute"/> changed. 
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Delegate for the CanExecute method that’s specified by ICommand. 
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <returns>
        /// True if predicate is null or returns true.
        /// </returns>
        public bool CanExecute(object parameter) => this.canExecute == null || this.canExecute(parameter);

        /// <summary>
        /// Method for parametrized execution of command action.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public void Execute(object parameter) => this.execute(parameter);

        /// <summary>
        /// Creates new instance of <see cref="RelayCommand"/> with given execute action and predicate. 
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="canExecute">Action executes if this predicate return true</param>
        /// <param name="execute">Action that should be perfomed</param>
        public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }
    }
}