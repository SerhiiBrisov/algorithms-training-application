﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace TrainingApp.Business.Models
{
    /// <summary>
    /// Model to sort it, generic type should be Comparable.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class ComparableModel<T> : INotifyPropertyChanged
    {
        /// <summary>
        /// Observable collection that modifies when services changes it.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>Collection of comparable objects.</value>
        private List<T> sortingCollection;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComparableModel{T}"/> class.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public ComparableModel() => this.SortingCollection = new List<T>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ComparableModel{T}"/> class.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="collection">The collection that should be sorted.</param>
        public ComparableModel(IEnumerable<T> collection) => this.sortingCollection = new List<T>(collection);

        /// <summary>
        /// Default length of sortingCollection
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public readonly int DefaultLength = 10;

        /// <summary>
        /// Method called when property changes 
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public void OnPropertyChanged([CallerMemberName] string prop = "") =>
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        /// <summary>
        /// Event that notified system about changes in properties.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the sorting collection.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>
        /// The sorting collection.
        /// </value>
        public List<T> SortingCollection
        {
            get => this.sortingCollection;

            set
            {
                this.sortingCollection = value;
                this.OnPropertyChanged(nameof(this.SortingCollection));
            }
        }

        /// <summary>
        /// Override method of class object to simplify output.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <returns>String of values stored in sortingCollection separeted by spaces.</returns>
        public override string ToString()
        {
            StringBuilder result = new StringBuilder(sortingCollection.Count);
            foreach (var item in sortingCollection)
            {
                result.Append(item.ToString());
                result.Append(" ");
            }
            return result.ToString().Trim();
        }
    }
}