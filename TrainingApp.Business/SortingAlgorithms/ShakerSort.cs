﻿using System.Collections.Generic;
using System.Threading;
using TrainingApp.Business.Interfaces;
using TrainingApp.Business.Models;

namespace TrainingApp.Business.Sortings
{
    /// <summary>
    /// Class implements Shaker sorting algorithm.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class ShakerSort<T> : ISorting<T>
    {
        /// <summary>
        /// Sorting method.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="entity">Model includes generic collection that will be sorted.</param>
        /// <param name="comparer">Custom comparer necessary to make collection sorted.</param>
        /// <param name="delay">Number of miliseconds of delay on every iteration.</param>
        public void Sort(ComparableModel<T> entity, IComparer<T> comparer, int delay)
        {
            int left = 0,
                right = entity.SortingCollection.Count - 1,
                count = 0;
            while (left < right)
            {
                for (int i = left; i < right; i++)
                {
                    count++;
                    if (comparer.Compare(entity.SortingCollection[i], entity.SortingCollection[i + 1]) > 0)
                    {
                        T tmp = entity.SortingCollection[i + 1];
                        entity.SortingCollection[i + 1] = entity.SortingCollection[i];
                        entity.SortingCollection[i] = tmp;
                    }
                }
                for (int i = right; i > left; i--)
                {
                    count++;
                    if (comparer.Compare(entity.SortingCollection[i - 1], entity.SortingCollection[i]) > 0)
                    {
                        T tmp = entity.SortingCollection[i - 1];
                        entity.SortingCollection[i - 1] = entity.SortingCollection[i];
                        entity.SortingCollection[i] = tmp;
                    }
                }
                left++;
                Thread.Sleep(delay);
            }
        }
    }
}