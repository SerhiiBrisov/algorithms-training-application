﻿using System.Collections.Generic;
using System.Threading;
using TrainingApp.Business.Interfaces;
using TrainingApp.Business.Models;

namespace TrainingApp.Business.Sortings
{
    /// <summary>
    /// Class implements Insertion sorting algorithm.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class InsertionSort<T> : ISorting<T>
    {
        /// <summary>
        /// Sorting method.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="entity">Model includes generic collection that will be sorted.</param>
        /// <param name="comparer">Custom comparer necessary to make collection sorted.</param>
        /// <param name="delay">Number of miliseconds of delay on every iteration.</param>
        public void Sort(ComparableModel<T> entity, IComparer<T> comparer, int delay)
        {
            int counter = 0;
            for (int i = 1; i < entity.SortingCollection.Count; i++)
            {
                for (int j = i; j > 0 && comparer.Compare(entity.SortingCollection[j - 1], entity.SortingCollection[j]) > 0; j--)
                {
                    counter++;
                    T tmp = entity.SortingCollection[j - 1];
                    entity.SortingCollection[j - 1] = entity.SortingCollection[j];
                    entity.SortingCollection[j] = tmp;
                    Thread.Sleep(delay);
                }
            }
        }
    }
}