﻿using System.Collections.Generic;
using System.Threading;
using TrainingApp.Business.Interfaces;
using TrainingApp.Business.Models;

namespace TrainingApp.Business.Sortings
{
    /// <summary>
    /// Class implements Bubble sorting algorithm.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class BubbleSort<T> : ISorting<T>
    {
        /// <summary>
        /// Sorting method.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="entity">Model includes generic collection that will be sorted..</param>
        /// <param name="comparer">Custom comparer necessary to make collection sorted</param>
        /// <param name="delay">Number of miliseconds of delay on every iteration.</param>
        public void Sort(ComparableModel<T> entity, IComparer<T> comparer, int delay)
        {
            for (int i = 0; i < entity.SortingCollection.Count; i++)
            {
                for (int j = i + 1; j < entity.SortingCollection.Count; j++)
                {
                    if (comparer.Compare(entity.SortingCollection[j], entity.SortingCollection[i]) < 0)
                    {
                        T temp = entity.SortingCollection[j];
                        entity.SortingCollection[j] = entity.SortingCollection[i];
                        entity.SortingCollection[i] = temp;
                    }
                    Thread.Sleep(delay);
                }
            }
        }
    }
}