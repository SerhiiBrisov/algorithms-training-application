﻿using System;

namespace TrainingApp.Business.DataStructures.BinaryTree
{
    /// <summary>
    /// Class includes event args for <see cref="BinaryTree{T}"/>.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    /// <typeparam name="T"></typeparam>
    public class TreeEventArgs<T> : EventArgs
    {
        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>
        /// The value.
        /// </value>
        public T Value { get; private set; }

        /// <summary>
        /// Gets the message.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>
        /// The message.
        /// </value>
        public string Message { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeEventArgs{T}"/> class.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="value">The value.</param>
        /// <param name="message">The message.</param>
        public TreeEventArgs(T value, string message)
        {
            this.Value = value;
            this.Message = message;
        }
    }
}