﻿namespace TrainingApp.Business.DataStructures.BinaryTree
{
    /// <summary>
    /// Enum of traversing type.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public enum TraverseType
    {
        InOrder,
        PreOrder,
        PostOrder
    }
}