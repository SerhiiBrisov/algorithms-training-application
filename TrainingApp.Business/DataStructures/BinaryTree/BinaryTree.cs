using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TrainingApp.Business.DataStructures.BinaryTree
{
    /// <summary>
    /// Class implementing data structure of Binary Tree.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class BinaryTree<T> : IEnumerable<T>
    {
        /// <summary>
        /// Handles events for adding and removing elements.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="sender">Instance of <see cref="BinaryTree<T>"/> that called the event</param>.
        /// <param name="args">Arguments passed by sender for subscribers.</param>
        public delegate void TreeEventHandler(object sender, TreeEventArgs<T> args);

        /// <summary>
        /// Event that should be called when new element is added.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public event TreeEventHandler ElementAdded;

        /// <summary>
        /// Event that should be called when element in tree is removed.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public event TreeEventHandler ElementRemoved;

        /// <summary>
        /// Field that includes root instance of node.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private BinaryTreeNode<T> root;

        /// <summary>
        /// Adds element to the tree according to comparer.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="item">Object that should be added in tree.</param>
        /// <exception cref="ArgumentNullException">Thrown if parameter was null.</exception>
        public void Add(T item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            var node = new BinaryTreeNode<T>(item);
            var eventArg = new TreeEventArgs<T>(item, "Item has been added");

            if (root == null)
                root = node;
            else
            {
                BinaryTreeNode<T> current = root, parent = null;

                while (current != null)
                {
                    parent = current;
                    if (this.Comparer.Compare(item, current.Value) < 0) current = current.Left;
                    else current = current.Right;
                }

                if (this.Comparer.Compare(item, parent.Value) < 0) parent.Left = node;
                else parent.Right = node;
            }
            this.Count++;
            this.ElementAdded?.Invoke(this, eventArg);
        }

        /// <summary>
        /// Checks if type T implements <see cref="IComparable<T>"/
        /// If it does: saves and uses as default comparer.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <exception cref="ArgumentException">Thrown when T doesn't implement <see cref="IComparable<T>".</exception
        public BinaryTree()
        {
            if (typeof(T).GetInterfaces().Contains(typeof(IComparable<T>)))
                this.Comparer = Comparer<T>.Default;
            else
                throw new ArgumentException("T not implemented IComparable<T>");
        }

        /// <summary>
        /// Creates instance of tree and saves custom comparer passed by parameter.
        /// </summary>
        /// <param name="comparer"><see cref="IComparer<T>"/>.</param>
        /// <owner>Serhii Brysov</owner>
        public BinaryTree(IComparer<T> comparer) => this.Comparer = comparer;

        /// <summary>
        /// Defines how many elements tree contains.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>Countity of nodes in tree.</value>
        public int Count { get; private set; }

        /// <summary>
        /// Comparator for comparison if Generic type in tree is complex class.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>Comparer of type.</value>
        public IComparer<T> Comparer { get; }

        /// <summary>
        /// Checks if tree contains element by its reference.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="item">Object that should (or not) be found in tree.</param>
        /// <returns>True if tree contains item, false if it doesn't.</returns> 
        public bool Contains(T data) => this.FindWithParent(data, out _) != null;

        /// Finds and returns first node with exact value. If value
        /// not found, returns null. Also returns parent of founded node (or null).
        /// Implemented for using in methods Contains and Remove.
        /// <owner>Serhii Brysov</owner>
        private BinaryTreeNode<T> FindWithParent(T value, out BinaryTreeNode<T> parent)
        {
            BinaryTreeNode<T> current = root;
            parent = null;
            //
            // While not found...
            //
            while (current != null)
            {
                int result = this.Comparer.Compare(current.Value, value);

                if (result > 0)
                {
                    //
                    // If value is lower, go left.
                    //
                    parent = current;
                    current = current.Left;
                }
                else if (result < 0)
                {
                    //
                    // If value is higher, go right.
                    //
                    parent = current;
                    current = current.Right;
                }
                else
                    //
                    // If equals -> break
                    //
                    break;
            }
            return current;
        }

        /// <summary>
        /// Makes in-order traverse
        /// Serves as a default <see cref="TraverseType"/> for tree.
        /// </summary>
        /// <returns>Enumerator for iterations in foreach cycle.</returns>
        /// <owner>Serhii Brysov</owner>
        public IEnumerator<T> GetEnumerator() => this.InOrderTraversal();

        /// <summary>
        /// Get enumerator for iterations in foreach cycle.
        /// </summary>
        /// <returns>Enumerator for iterations in foreach cycle.</returns>
        /// <owner>Serhii Brysov</owner>
        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        /// <summary>
        /// Method implements traversal in order.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <returns>Enumerator needed to walk throw all tree nodes in order.</returns>
        public IEnumerator<T> InOrderTraversal()
        {
            //
            // This is non-recursive algorithm
            // It uses stack to avoid recursion
            //
            if (root != null)
            {
                //
                // Stack to save missing nodes
                //
                Stack stack = new Stack();
                BinaryTreeNode<T> current = root;
                //
                // When we avoid recursion, we have to  
                // remember, which side we are moving
                //
                bool goLeftNext = true;
                //
                // Put root into stack
                //
                stack.Push(current);
                while (stack.Count > 0)
                {
                    //
                    // If we go left
                    //
                    if (goLeftNext)
                    {
                        //
                        // Put all, except last left node into stack.
                        // Last left node we'll return with yield.
                        //
                        while (current.Left != null)
                        {
                            stack.Push(current);
                            current = current.Left;
                        }
                    }
                    //
                    // InOrder Traversal : left->yield->right.
                    //
                    yield return current.Value;
                    //
                    // If we have right nodes, we go.
                    //
                    if (current.Right != null)
                    {
                        current = current.Right;
                        //
                        // After we went right one time,
                        // we need to go left again.
                        //
                        goLeftNext = true;
                    }
                    else
                    {
                        //
                        // If we cant go to right path, we need to take parent node
                        // from stack, explore it and go to its right child.
                        //
                        current = stack.Pop() as BinaryTreeNode<T>;
                        goLeftNext = false;
                    }
                }
            }
        }

        /// <summary>
        /// Makes tree traverse and fills collection according traversal type.
        /// </summary>
        /// <param name="container">Container that will be filled with elements of tree.</param>
        /// <returns>Sequense of elements of tree according to in order traversal type.</returns>
        /// <owner>Serhii Brysov</owner>
        private void InOrderTraversal(List<T> container) => this.InOrderTraversal(container, root);

        /// <summary>
        /// Recursive method makes tree traverse and fills collection with another node on every iteration.
        /// </summary>
        /// <param name="container">Container that will be filled with elements of tree.</param>
        /// <param name="node">Current tree node from which traverse continues.</param>
        /// <returns>Sequense of elements of tree according to in order traversal type with addition of next node.</returns>
        /// <owner>Serhii Brysov</owner>
        private void InOrderTraversal(List<T> container, BinaryTreeNode<T> node)
        {
            if (node != null)
            {
                this.InOrderTraversal(container, node.Left);
                container.Add(node.Value);
                this.InOrderTraversal(container, node.Right);
            }
        }

        /// <summary>
        /// Makes tree traverse and fills collection according traversal type.
        /// </summary>
        /// <param name="container">Container that will be filled with elements of tree.</param>
        /// <returns>Sequense of elements of tree according to post order traversal type.</returns>
        /// <owner>Serhii Brysov</owner>
        private void PostOrderTraversal(List<T> container) =>
            this.PostOrderTraversal(container, root);

        /// <summary>
        /// Recursive method makes tree traverse and fills collection with another node on every iteration.
        /// </summary>
        /// <param name="container">Container that will be filled with elements of tree.</param>
        /// <param name="node">Current tree node from which traverse continues.</param>
        /// <returns>Sequense of elements of tree according to post order traversal type with addition of next node.</returns>
        /// <owner>Serhii Brysov</owner>
        private void PostOrderTraversal(List<T> container, BinaryTreeNode<T> node)
        {
            if (node != null)
            {
                this.PostOrderTraversal(container, node.Left);
                this.PostOrderTraversal(container, node.Right);
                container.Add(node.Value);
            }
        }

        /// <summary>
        /// Makes tree traverse and fills collection according traversal type.
        /// </summary>
        /// <param name="container">Container that will be filled with elements of tree.</param>
        /// <returns>Sequense of elements of tree according to pre order traversal type.</returns>
        /// <owner>Serhii Brysov</owner>
        private void PreOrderTraversal(List<T> container) => this.PreOrderTraversal(container, root);

        /// <summary>
        /// Recursive method makes tree traverse and fills collection with another node on every iteration.
        /// </summary>
        /// <param name="container">Container that will be filled with elements of tree.</param>
        /// <param name="node">Current tree node from which traverse continues.</param>
        /// <returns>Sequense of elements of tree according to pre order traversal type with addition of next node.</returns>
        /// <owner>Serhii Brysov</owner>
        private void PreOrderTraversal(List<T> container, BinaryTreeNode<T> node)
        {
            if (node != null)
            {
                container.Add(node.Value);
                this.PreOrderTraversal(container, node.Left);
                this.PreOrderTraversal(container, node.Right);
            }
        }

        /// <summary>
        /// Removes element from tree by its reference.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="item">Object that should be removed from tree.</param>
        /// <returns>True if element was deleted succesfully, false if element wasn't found in tree.</returns>
        public bool Remove(T item)
        {
            BinaryTreeNode<T> current;
            //
            // Find removal node
            //
            current = this.FindWithParent(item, out BinaryTreeNode<T> parent);

            if (current == null)
                return false;

            this.Count--;
            //
            // Case 1: If there are no childs on right, left child will put on deleting place.
            //
            if (current.Right == null)
            {
                if (parent == null)
                    root = current.Left;
                else
                {
                    int result = this.Comparer.Compare(parent.Value, current.Value);
                    if (result > 0)
                        parent.Left = current.Left;
                    else if (result < 0)
                        parent.Right = current.Left;
                }
            }
            //
            //  Case 2: If right child has no childs on left side, so it will put on deleting place.
            //
            else if (current.Right.Left == null)
            {
                current.Right.Left = current.Left; if (parent == null) { root = current.Right; }
                else
                {
                    int result = this.Comparer.Compare(parent.Value, current.Value);
                    if (result > 0)
                        parent.Left = current.Right;
                    else if (result < 0)
                        //
                        parent.Right = current.Right;
                }
            }
            //
            // Case 3: If right child has childs on left side, so last left child
            // from right subtree will put on deleting place. 
            //
            else
            {   //
                // Find last left node. 
                //
                BinaryTreeNode<T> leftmost = current.Right.Left;
                BinaryTreeNode<T> leftmostParent = current.Right;
                while (leftmost.Left != null)
                {
                    leftmostParent = leftmost;
                    leftmost = leftmost.Left;
                }
                //
                // Left subtree of parent becomes right subtree of last left node.
                //
                leftmostParent.Left = leftmost.Right;
                //
                // Left or right child of current node becomes Left or right child of the last left node. 
                //
                leftmost.Left = current.Left;
                leftmost.Right = current.Right;
                if (parent == null)
                {
                    root = leftmost;
                }
                else
                {
                    int result = this.Comparer.Compare(parent.Value, current.Value);
                    if (result > 0)
                        parent.Left = leftmost;
                    else if (result < 0)
                        parent.Right = leftmost;
                }
            }
            var eventArg = new TreeEventArgs<T>(item, "Item has been removed");
            this.ElementRemoved?.Invoke(this, eventArg);
            return true;
        }

        /// <summary>
        /// Returns item with the highest value.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <returns>The element with highest value.</returns>
        /// <exception cref="InvalidOperationException">Thrown if tree is empty.</exception> 
        public T TreeMax()
        {
            if (root == null)
                throw new InvalidOperationException();
            return this.TreeMax(root);
        }

        /// <summary>
        /// Returns item with the lowest value.
        /// </summary>
        /// <returns>The element with lowest value.</returns>
        /// <exception cref="InvalidOperationException">Thrown if tree is empty.</exception>
        /// <owner>Serhii Brysov</owner>
        public T TreeMin()
        {
            if (root == null)
                throw new InvalidOperationException();
            return this.TreeMin(root);
        }

        /// <summary>
        /// Makes tree traversal.
        /// </summary>
        /// <param name="traverseType"><see cref="TraverseType">.</param>
        /// <returns>Sequense of elements of tree according to traverse type.</returns>
        /// <owner>Serhii Brysov</owner>
        public IEnumerable<T> Traverse(TraverseType traverseType)
        {
            List<T> res = new List<T>();
            switch (traverseType)
            {
                case TraverseType.InOrder:
                    this.InOrderTraversal(res);
                    return res.ToArray<T>();
                case TraverseType.PreOrder:
                    this.PreOrderTraversal(res);
                    return res.ToArray<T>();
                case TraverseType.PostOrder:
                    this.PostOrderTraversal(res);
                    return res.ToArray<T>();
                default:
                    throw new ArgumentException("traverseType");
            }
        }

        /// <summary>
        /// Recursive method that traverse all tree nodes.
        /// </summary>
        /// <returns>The element with highest value.</returns>
        /// <owner>Serhii Brysov</owner>
        private T TreeMax(BinaryTreeNode<T> node)
        {
            if (node == null)
                return default;
            T res = node.Value;
            T lres = this.TreeMax(node.Left);
            T rres = this.TreeMax(node.Right);
            if (!((lres is null) || rres.Equals(default(T))) && this.Comparer.Compare(lres, res) > 0)
                res = lres;
            if (!((rres is null) || rres.Equals(default(T))) && this.Comparer.Compare(rres, res) > 0)
                res = rres;
            return res;
        }

        /// <summary>
        /// Recursive method that traverse all tree nodes.
        /// </summary>
        /// <returns>The element with lowest value.</returns>
        /// <owner>Serhii Brysov</owner>
        private T TreeMin(BinaryTreeNode<T> node)
        {
            if (node == null)
                return default;
            T res = node.Value;
            T rres = this.TreeMin(node.Right);
            T lres = this.TreeMin(node.Left);
            if (!((lres is null) || rres.Equals(default(T))) && this.Comparer.Compare(lres, res) < 0)
                res = lres;
            if (!((rres is null) || rres.Equals(default(T))) && this.Comparer.Compare(rres, res) < 0)
                res = rres;
            return res;
        }
    }
}