﻿namespace TrainingApp.Business.DataStructures.BinaryTree
{
    /// <summary>
    /// Class implementing node of Binary Tree.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class BinaryTreeNode<T>
    {
        /// <summary>
        /// Creates instance of node and saves parameter value.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="value">Value to be saved</param>
        public BinaryTreeNode(T value) => this.Value = value;

        /// <summary>
        /// Points to the bottom left node.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>Child left node.</value>
        public BinaryTreeNode<T> Left { get; set; }

        /// <summary>
        /// Points to the bottom right node.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>Child right node.</value>
        public BinaryTreeNode<T> Right { get; set; }

        /// <summary>
        /// Keeps data stored in this node.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>Value of this node.</value>
        public T Value { get; private set; }        
    }
}