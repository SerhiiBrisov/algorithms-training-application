﻿namespace TrainingApp.Business.DataStructures.List
{
    /// <summary>
    /// Class implementing node of Linked list.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class Item<T>
    {
        /// <summary>
        /// Keeps value stored in this node..
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>Value of this node.</value>
        public T Data { get; set; }

        /// <summary>
        /// Creates instance of list node and saves parameter value
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="data">Value to be saved</param>
        public Item(T data) => this.Data = data;

        /// <summary>
        /// Points to the next node.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>Next node.</value>
        public Item<T> Next { get; set; }        
    }
}