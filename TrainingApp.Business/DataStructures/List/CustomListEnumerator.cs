﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TrainingApp.Business.DataStructures.List
{
    /// <summary>
    /// Class enumerator for custom linked list.
    /// Implement IDisposable, which is also implemented by IEnumerator(T).
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    internal class CustomListEnumerator<T> : IEnumerator<T>
    {
        /// <summary>
        /// Field that includes linked list that will be enumerate.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private CustomList<T> list;

        /// <summary>
        /// Position of iterator
        /// <remarks>By default is -1</remarks>
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private int position = -1;

        /// <summary>
        /// Flag wich become true when list is disposed.
        /// <remarks>
        /// By default is false.
        /// </remarks>
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private bool disposedValue = false;

        /// <summary>
        /// Get current item in list.
        /// </summary>
        /// <remarks>
        /// Realisation of IEnumerator interface.
        /// </remarks>
        /// <owner>Serhii Brysov</owner>
        /// <value>Current item in list.</value>
        public T Current
        {
            get
            {
                if (this.position == -1 || this.position >= this.list.Count)
                    throw new InvalidOperationException();
                return this.list[this.position];
            }
        }

        /// <summary>
        /// Create enumerator for linked list passed as param.
        /// </summary>
        /// <param name="list"><see cref="CustomList<T>"/></param>
        /// <owner>Serhii Brysov</owner>
        public CustomListEnumerator(CustomList<T> list) => this.list = list;

        /// <summary>
        /// Method that makes Garbage Collector finalize this item.
        /// </summary>
        /// <remarks>
        /// Realisation of IDisposable interface.
        /// </remarks>
        /// <owner>Serhii Brysov</owner>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Method that clears memory from this object.
        /// </summary>
        /// <param name="disposing">
        /// If true - list is already disposed.
        /// If false - field with reference to list will be clear, and fields will take default values.
        /// </param>
        /// <owner>Serhii Brysov</owner>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue && disposing)
            {
                this.position = -1;
                this.list = null;
            }
            this.disposedValue = true;
        }

        /// <summary>
        /// Get object instance of current item in list.
        /// </summary>
        /// <remarks>
        /// Realisation of IEnumerator interface.
        /// </remarks>
        /// <owner>Serhii Brysov</owner>
        /// <value>Object instance of current item in list.</value>
        object IEnumerator.Current => Current;        

        /// <summary>
        /// Method define if possible to move to next item in linked list.
        /// </summary>
        /// <returns>
        /// True if iterator can move to next item.
        /// False if current item is tail of list.
        /// </returns>
        /// <owner>Serhii Brysov</owner>
        public bool MoveNext()
        {
            if (this.position < this.list.Count - 1)
            {
                this.position++;
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Method resets position of traverse.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public void Reset() => this.position = -1;  

        /// <summary>
        /// Finalizes an instance of the <see cref="CustomListEnumerator{T}"/> class.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        ~CustomListEnumerator() => this.Dispose(false);
    }
}