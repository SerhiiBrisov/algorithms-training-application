﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TrainingApp.Business.DataStructures.List
{
    /// <summary>
    /// Class implementing data structure of Linked List.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class CustomList<T> : IList<T>
    {
        /// <summary>
        /// The tail of linked list
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private Item<T> tail;

        /// <summary>
        /// The property return number of elements contained in the CustomList.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private int count;

        /// <summary>
        ///  Adds an object to the end of the CustomList.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="item">Object that should be added in the CustomList.</param>
        /// <exception cref="ArgumentNullException">Throws when you try to add null.</exception>
        public void Add(T item)
        {
            if (item is null)
                throw new ArgumentNullException("item");
            Item<T> data = new Item<T>(item);
            if (Head == null)
                this.Head = data;
            else
                this.tail.Next = data;
            this.tail = data;
            this.count++;
        }

        /// <summary>
        /// Checks if values are nulls.
        /// </summary>
        /// <overloads>
        /// <summary>This method has two overloads.</summary>
        /// <remarks>Use the method with array in parameters when need to check more than one arguments.</remarks>
        /// <example>
        /// <code>
        /// // Check for null array of values.
        /// this.ArgumentNullCheck(parameterName, array);
        ///
        /// // Check for null one value.
        /// this.ArgumentNullCheck(parameterName, value);
        ///
        /// </code>
        /// </example>
        /// </overloads>
        /// <param name="name">Argument name</param>
        /// <param name="values">Values need to check</param>
        /// <exception cref="ArgumentNullException">Thrown when param value is null</exception>
        /// <owner>Serhii Brysov</owner>
        private void ArgumentNullCheck(string name, T[] values)
        {
            if (values is null)
                throw new ArgumentNullException(name);
        }

        /// <summary>
        /// Checks if value is null.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="name">Argument name</param>
        /// <param name="value">Value need to check</param>
        /// <exception cref="ArgumentNullException">Thrown when param value is null.</exception>
        private void ArgumentNullCheck(string name, T value)
        {
            if (value is null)
                throw new ArgumentNullException(name);
        }

        /// <summary>
        /// Removes all elements from the CustomList.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public void Clear()
        {
            this.Head = null;
            this.tail = null;
            this.count = 0;
        }

        /// <summary>
        /// Determines whether an element is in the CustomList.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="item">Object we check to see if it is on the CustomLIst.</param>
        /// <returns>True if the element exists in the CustomList, else false.</returns>
        public bool Contains(T item)
        {
            Item<T> current = this.Head;
            while (current != null)
            {
                if (current.Data.Equals(item))
                    return true;
                current = current.Next;
            }
            return false;
        }

        /// <summary>
        /// Copies the entire CustomList to a compatible one-dimensional array, starting at the beginning of the target array.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="array">The one-dimensional System.Array that is the destination of the elements copied
        ///     from CustomList.</param>
        /// <param name="arrayIndex">The zero-based index in the source System.Array at which
        ///   copying begins.</param>
        ///   <exception cref="ArgumentNullException">Array is null.</exception>
        ///   <exception cref="ArgumentException">The number of elements in the source CustomList is greater
        ///    than the number of elements that the destination array can contain.</exception>
        public void CopyTo(T[] array, int arrayIndex)
        {
            Type myType = Type.GetType(this.ToString());
            var parameters = myType.GetMethod("CopyTo").GetParameters();
            this.ArgumentNullCheck(parameters[0].Name, array);

            if (array.Length < count)
                throw new ArgumentException("Array Length is smaller than sourse.count + arrayIndex", "array", null);
            foreach (var item in this)
            {
                array[arrayIndex] = item;
                arrayIndex++;
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="CustomList{T}" />.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public int Count
        {
            get => this.count;
            private set
            {
                if (value >= 0)
                    this.count = value;
            }
        }

        /// <summary>
        /// Constructor that gets params T as parameter. 
        /// </summary>
        /// <exception cref="ArgumentNullException">Thrown when values is null</exception>
        /// <param name="values"></param>
        /// <owner>Serhii Brysov</owner>
        public CustomList(params T[] values)
        {
            if (values is null)
                throw new ArgumentNullException("values");

            for (int i = 0; i < values.Length; i++)
                this.Add(values[i]);
        }

        /// <summary>
        /// Constructor that gets IEnumerable collection as parameter.      
        /// </summary>
        ///<exception cref="ArgumentNullException">Thrown when values is null.</exception>
        /// <param name="values"></param>
        /// <owner>Serhii Brysov</owner>
        public CustomList(IEnumerable<T> values)
        {
            if (values is null)
                throw new ArgumentNullException("values");

            for (int i = 0; i < values.Count(); i++)
                this.Add(values.ElementAt(i));
        }

        /// <summary>
        /// Returns an enumerator that iterates through the CustomList.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <returns></returns>
        public IEnumerator<T> GetEnumerator() => new CustomListEnumerator<T>(this);

        /// <summary>
        /// The property returns first element of list.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>First element of list</value>
        public Item<T> Head { get; private set; }

        /// <summary>
        /// Get enumerator for iterations in foreach cycle.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <returns>Enumerator for iterations in foreach cycle.</returns>
        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        /// <summary>
        /// Searches for the specified object and returns the zero-based index of the first.
        /// occurrence within the CustomList.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="item">The object whose index we want to get. </param>
        /// <returns>The zero-based index of the first occurrence of item within the entire CustomList,
        ///    if found; otherwise, -1.</returns>
        public int IndexOf(T item)
        {
            Item<T> current = Head;
            int counter = 0;
            while (current != null)
            {
                if (current.Data.Equals(item))
                    return counter;
                current = current.Next;
                counter++;
            }
            return -1;
        }

        /// <summary>
        /// Inserts an element into the CustomList at the specified index.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="index">The zero-based index at which item should be inserted.</param>
        /// <param name="item">The object to insert.</param>
        /// <exception cref="ArgumentOutOfRangeException">Throw when index is less than 0 or greater than Count - 1.</exception>
        /// <exception cref="ArgumentNullException">Thrown when item is null.</exception>
        public void Insert(int index, T item)
        {
            if (index < 0 || index > count)
                throw new ArgumentOutOfRangeException("index");
            if (item is null)
                throw new ArgumentNullException("item");
            if (index == count)
            {
                this.Add(item);
                return;
            }
            int counter = 0;
            Item<T> currentNode = Head;
            Item<T> prevNode = null;
            while (counter != index)
            {
                prevNode = currentNode;
                currentNode = currentNode.Next;
                counter++;
            }
            Item<T> newItem = new Item<T>(item);
            if (prevNode is null)
                this.Head = newItem;
            else
                prevNode.Next = newItem;
            newItem.Next = currentNode;
            this.count++;
        }

        /// <summary>
        /// Gets a value indicating whether the IList is read-only.
        /// Make it always false
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public bool IsReadOnly => false;

        /// <summary>
        /// Removes the first occurrence of a specific object from the CustomList.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="item"> The object to remove from the CustomList.</param>
        /// <returns>True if item is successfully removed; otherwise, false. This method also returns
        ///     false if item was not found in the CustomList.</returns>
        /// <exception cref="ArgumentNullException">Throws when you try to remove null.</exception>
        public bool Remove(T item)
        {
            Type myType = Type.GetType(this.ToString());
            var parameters = myType.GetMethod("Remove").GetParameters();
            this.ArgumentNullCheck(parameters[0].Name, item);
            Item<T> current = this.Head;
            Item<T> previous = null;
            while (current != null)
            {
                if (current.Data.Equals(item))
                {
                    if (previous != null)
                    {
                        previous.Next = current.Next;
                        if (current.Next == null)
                            this.tail = previous;
                    }
                    else
                    {
                        this.Head = Head.Next;
                        if (this.Head == null)
                            this.tail = null;
                    }
                    this.count--;
                    return true;
                }
                previous = current;
                current = current.Next;
            }
            return false;
        }

        /// <summary>
        /// Removes the element at the specified index of the CustomList.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="index">The zero-based index of the element to remove.</param>
        /// <exception cref="ArgumentOutOfRangeException">Throw when index is less than 0 or greater than Count - 1.</exception>
        public void RemoveAt(int index)
        {
            if (index < 0 || index >= count)
                throw new ArgumentOutOfRangeException("index");
            this.Remove(this[index]);
        }

        /// <summary>
        /// Get or set data at the position.
        /// </summary>
        /// <param name="index">Position</param>
        /// <exception cref="IndexOutOfRangeException">Throw when index is less than 0 or greater than Count - 1.</exception>
        /// <owner>Serhii Brysov</owner>
        public T this[int index]
        {
            get
            {
                if (index < 0 || index >= count)
                {
                    T[] array = new T[count];
                    _ = array[count + 1].ToString();
                }
                int counter = 0;
                Item<T> data = Head;
                while (counter != index)
                {
                    data = data.Next;
                    counter++;
                }
                return data.Data;
            }
            set
            {
                if (index < 0 || index >= count)
                {
                    T[] array = new T[count];
                    _ = array[count + 1].ToString();
                }
                int counter = 0;
                Item<T> data = Head;
                while (counter != index)
                {
                    data = data.Next;
                    counter++;
                }
                data.Data = value;
            }
        }
    }
}