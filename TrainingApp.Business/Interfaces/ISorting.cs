﻿using System.Collections.Generic;
using TrainingApp.Business.Models;

namespace TrainingApp.Business.Interfaces
{
    /// <summary>
    /// All classes that includes sorting algorithm should implement this interface.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public interface ISorting<T>
    {
        /// <summary>
        /// Pattern of method of sorting.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="entity">Model includes generic collection that will be sorted.</param>
        /// <param name="comparer">Custom comparer necessary to make collection sorted.</param>
        /// <param name="delay">Number of miliseconds of delay on every iteration.</param>
        void Sort(ComparableModel<T> entity, IComparer<T> comparer, int delay);
    }
}