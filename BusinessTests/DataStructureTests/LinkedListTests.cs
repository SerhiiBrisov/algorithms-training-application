﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TrainingApp.Business.DataStructures.List;

namespace BusinessTests.DataStructureTests
{
    /// <summary>
    /// Represents the unit tests towards the <see cref="CustomList{T}"/> object.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    [TestClass]
    public class LinkedListTests
    {
        /// <summary>
        /// Performs a test towards the property <see cref="CustomList{T}.Count"/> of empty <see cref="CustomList{T}"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Clear_ReturnCount0()
        {
            //
            //arrange
            //
            CustomList<int> list = new CustomList<int>(1, 2, 3);
            Item<int> expectedHead = null;
            //
            //act
            //
            list.Clear();
            int count = list.Count;
            var actualHead = list.Head;
            //
            //arrange   
            //
            Assert.AreEqual(0, count, message: "Clear method works incorrectly ");
            Assert.AreEqual(expectedHead, actualHead, message: "Clear method works incorrectly ");
        }

        /// <summary>
        /// Performs a test towards the method <see cref="CustomList{T}.Contains(T)"/>. 
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DataRow(3, new int[] { 1, 2, 3, 4, 5 }, true)]
        [DataRow(100, new int[] { 1, 2, 3, 4, 5 }, false)]
        [DataRow(0, new int[] { }, false)]
        public void Contains_TrueOrFalseReturned(int elementsToFind, int[] elements, bool expected)
        {
            //
            //arrange
            //
            CustomList<int> list = new CustomList<int>(elements);
            //
            //act
            //
            var isContains = list.Contains(elementsToFind);
            //
            //arrange
            //
            Assert.AreEqual(expected, isContains, message: "Contains method works incorrectly ");
        }

        /// <summary>
        /// Performs a test towards the property Head in bundle with method <see cref="CustomList{T}.Remove(T)"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void CopyTo_ArrayIsNull_ThrowsArgumentNullException()
        {
            //
            //arrange
            //
            CustomList<int> list = new CustomList<int>(1, 2, 3, 4, 5);
            int[] array = null;
            var expectedEx = typeof(ArgumentNullException);
            //
            //act
            //
            var actEx = Assert.ThrowsException<ArgumentNullException>(() => list.CopyTo(array, 2));
            //
            //assert      
            //
            Assert.AreEqual(expectedEx, actEx.GetType(),
                message: "Copy To throws ArgumentNullException ,if array is null ");
        }

        /// <summary>
        /// Performs a test towards the method <see cref="CustomList{T}.CopyTo(T[], int)"/> that should return array with values.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [TestMethod]
        public void CopyTo_ReturnsArray()
        {
            //
            //arrange
            //
            CustomList<int> list = new CustomList<int>(1, 2, 3, 4, 5);
            int[] array = new int[10];
            int actual = 1;
            //
            //act
            //
            list.CopyTo(array, 2);
            //
            //assert
            //
            Assert.AreEqual(array[2], actual,
                message: "CopyTo works incorrectly");
        }

        /// <summary>
        /// Performs a test towards the method CopyTo that should throw <see cref="ArgumentException"/>
        /// quantity of elements in the source list greather than array capacity.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void CopyTo_ThrowsArgumentException()
        {
            //
            //arrange
            //
            CustomList<int> list = new CustomList<int>(1, 2, 3, 4, 5);
            int[] array = new int[2];
            var expectedEx = typeof(ArgumentException);
            //
            //act
            //
            var actEx = Assert.ThrowsException<ArgumentException>(() => list.CopyTo(array, 2));
            //
            //assert 
            //
            Assert.AreEqual(expectedEx, actEx.GetType(),
                message: "Copy to throws ArgumentException in case array has invalid size");
        }

        /// <summary>
        /// Performs a test towards the property <see cref="CustomList{T}.Count"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DataRow(1, 2, 3, 4, 5, 6)]
        [DataRow(0)]
        public void Count_Get_ShouldReturnCorrectValue(params int[] elements)
        {
            //
            //arrange
            //
            CustomList<int> list = new CustomList<int>(elements);
            //
            //act
            //
            var expected = elements.Length;
            var actual = list.Count;
            //
            //assert
            //
            Assert.AreEqual(expected, actual, message: "Length get property works incorrectly");
            Assert.AreEqual(elements[0], list[0], message: "Get indexer works incorrectly ");
        }
        /// <summary>
        /// Performs a test towards the property <see cref="CustomList{T}.Count"/> of new <see cref="CustomList{T}"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Count_Return0()
        {
            //
            // arrange
            //
            int zero = 0;
            //
            //act
            //
            CustomList<int> list = new CustomList<int>();
            //
            //assert 
            //
            Assert.AreEqual(list.Count, zero, message: "Count get property works incorrectly");
        }

        /// <summary>
        /// Performs a test towards creating new CustomList that should throw <see cref="ArgumentNullException"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void CreateCustomList_WithNullArray_ShouldThrowArgumentNullException()
        {
            //
            //Arrange
            //
            int[] array = null;
            var expectedEx = typeof(ArgumentNullException);
            //
            //Act
            //
            var actualEx = Assert.ThrowsException<ArgumentNullException>(() => new CustomList<int>(array));
            //
            //Assert
            //
            Assert.AreEqual(expectedEx, actualEx.GetType(), message: "Custom List can't be created in case of array is null");
        }

        /// <summary>
        /// Performs a test towards the <see cref="CustomListEnumerator{T}"/>, 
        /// CustomList should work correctly in foreach cycle.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void CreateCustomList_WithNullList_ShouldThrowArgumentNullException()
        {
            //
            //arrange
            //
            List<int> list = null;
            var expectedEx = typeof(ArgumentNullException);
            //
            //act
            //
            var actEx = Assert.ThrowsException<ArgumentNullException>(() => new CustomList<int>(list));
            //
            //assert
            //
            Assert.AreEqual(expectedEx, actEx.GetType(),
                message: "CustomList can't be created  in case of list is null ");
        }

        /// <summary>
        /// Performs a test towards the indexer.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DataRow(-1)]
        [DataRow(2)]
        public void Get_WrongIndex_ThrowsIndexOutOfRange(int index)
        {
            //
            //arrange
            //
            CustomList<int> list = new CustomList<int>(1);
            var expectedEx = typeof(IndexOutOfRangeException);
            //
            //act
            //
            var actEx = Assert.ThrowsException<IndexOutOfRangeException>(() => list[index].ToString());
            //
            //assert  
            //
            Assert.AreEqual(expectedEx, actEx.GetType(),
                message: "Indexer throws IndexOutOfRangeException in case index is out of list range  ");
        }

        /// <summary>
        /// Performs a test towards the method <see cref="CustomList{T}.GetEnumerator"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void GetEnumerator_OfListAndCustomList_ShouldHaveEqualElements()
        {
            //
            //arrange 
            //
            List<int> list = new List<int>() { 1, 2, 3 };
            CustomList<int> custom = new CustomList<int>(list);
            //
            //act
            //
            var en = list.GetEnumerator();
            var en2 = custom.GetEnumerator();
            //
            //assert
            //
            if (en.MoveNext() && en2.MoveNext())
                Assert.AreEqual(en.Current, en2.Current,
                    message: "GetEnumerator works incorrectly");
        }

        /// <summary>
        /// Performs a test towards the <see cref="CustomListEnumerator{T}", 
        /// <see cref="CustomList{T}"/> should work correctly in foreach cycle.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void GetEnumerator_UsingIEnumerableExplicitlyOfListAndCustomList_ShouldHaveEqualElements()
        {
            //
            //arrange 
            //
            List<int> list = new List<int>() { 1, 2, 3 };
            CustomList<int> custom = new CustomList<int>(list);
            List<int> actualList = new List<int>();
            //
            //act
            //
            IEnumerable enumerable = custom;
            foreach (int item in enumerable)
                actualList.Add(item);            
            //
            //assert
            //
            CollectionAssert.AreEqual(list, actualList, message: "GetEnumerator works incorrectly");
        }

        /// <summary>
        /// Performs a test towards the property Head in bundle with method <see cref="CustomList{T}.Add(T)"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DataRow(new int[] { 5, 2, 7, 10 })]
        [DataRow(new int[] { 9, 0 })]
        [DataRow(new int[] { 1, 1, 1, 1, 7, 5, 3 })]
        public void Head_AfterAddNewItem_ReturnSameCollection(int[] list)
        {
            //
            //Arrange
            //
            int addElement = 10;
            int? actualLast = null;
            CustomList<int> custom = new CustomList<int>(list);
            //
            //Act
            //
            var exHead = list[0];
            var actualHead = custom.Head;
            custom.Add(addElement);
            var current = actualHead;
            while (current != null)
            {
                actualLast = current.Data;
                current = current.Next;
            }
            //
            //Assert
            //
            Assert.IsTrue(actualHead != null);
            Assert.AreEqual(exHead, actualHead.Data);
            Assert.AreEqual(addElement, actualLast, message: "Head property works incorrect");
        }

        /// <summary>
        /// Performs a test towards the property Head in bundle with method <see cref="CustomList{T}.Remove(T)"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DataRow(new int[] { 5, 2, 7, 10 }, 5)]
        [DataRow(new int[] { 1, 1, 1 }, 1)]
        [DataRow(new int[] { -6, 0, 11, 55, -4 }, -4)]
        public void Head_AfterRemoveItem_ReturnSameCollection(int[] array, int removeElement)
        {
            //
            //Arrange
            //
            var list = array.ToList();
            List<int> actualList = new List<int>();
            //
            //Act
            //
            CustomList<int> custom = new CustomList<int>(list);
            custom.Remove(removeElement);
            list.Remove(removeElement);
            var current = custom.Head;
            while (current != null)
            {
                actualList.Add(current.Data);
                current = current.Next;
            }
            //
            //Assert
            //
            CollectionAssert.AreEqual(list, actualList, message: "Head property works incorrect ");
        }

        /// <summary>
        /// Performs a test towards the property <see cref="CustomList{T}.Head"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DataRow(new int[] { 5, 2, 7, 10 })]
        [DataRow(new int[] { 3, 1 })]
        [DataRow(new int[] { 55, 30, 1, 19, 6, 6 })]
        public void Head_ReturnElementsOfCollection(int[] list)
        {
            //
            //Arrange
            //
            var exHead = list[0];
            CustomList<int> custom = new CustomList<int>(list);
            var actualHead = custom.Head;
            var current = actualHead;
            List<int> actualList = new List<int>();
            //
            //Act
            //
            while (current != null)
            {
                actualList.Add(current.Data);
                current = current.Next;
            }
            //
            //Assert
            //
            Assert.IsTrue(actualHead != null);
            Assert.AreEqual(exHead, actualHead.Data);
            CollectionAssert.AreEqual(list, actualList, message: "Head property works incorrect ");
        }

        /// <summary>
        /// Performs a test towards the method <see cref="CustomList{T}.IndexOf(T)"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DataRow(2, new int[] { 1, 2, 3, 4, 2, 6 }, 1)]
        [DataRow(-10, new int[] { 1, 2, 3, 4, 5, 6 }, -1)]
        [DataRow(0, new int[] { }, -1)]
        public void IndexOf_Element_ExpectedPositionReturned(int element, int[] elements, int expected)
        {
            //
            //arrange
            //
            CustomList<int> list = new CustomList<int>(elements);
            //
            //act
            //
            var position = list.IndexOf(element);
            //
            //arrange
            //
            Assert.AreEqual(expected, position, message: "IndexOf method works incorrectly");
        }

        /// <summary>
        /// Performs a test towards the method Insert with wrong value that should throw <see cref="ArgumentNullException"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Insert_AtPositionNullValue_ThrowsArgumentsNullException()
        {
            //
            //arrange
            //
            CustomList<object> list = new CustomList<object>();
            var expectedEx = typeof(ArgumentNullException);
            //
            //act
            //
            var actEx = Assert.ThrowsException<ArgumentNullException>(() => list.Insert(0, null));
            //
            //assert  
            //
            Assert.AreEqual(expectedEx, actEx.GetType(),
                message: "Insert throws ArgumentNullException in case  item is null");
        }

        /// <summary>
        /// Performs a test towards the method Insert that should change property <see cref="CustomList{T}.Count"/>. 
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DataRow(0)]
        [DataRow(5)]
        [DataRow(2)]
        public void Insert_AtPositionValue_ReturnCount(int position)
        {
            //
            //arrange
            //
            CustomList<int> list = new CustomList<int>(1, 2, 7, 8, 10);
            //
            //act
            //
            int elementToInsert = 100;
            list.Insert(position, elementToInsert);
            int count = list.Count;
            int actualValue = list[position];
            //
            //arrange
            //
            Assert.AreEqual(elementToInsert, actualValue, message: "Insert or Get Count  work incorrectly ");
            Assert.AreEqual(6, count, message: "Insert or Get Count  work incorrectly ");
        }

        /// <summary>
        /// Performs a test towards the method Remove that should change property <see cref="CustomList{T}.Count"/>. 
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DataRow(2, new int[] { 1, 2, 3, 4, 5 }, 4, true)]
        [DataRow(10, new int[] { 1, 2, 3, 4, 5 }, 5, false)]
        [DataRow(15, new int[] { 11, 4, 5 }, 3, false)]
        [DataRow(-1, new int[] { -1, 2, -1, 4, 5 }, 4, true)]
        [DataRow(99, new int[] { -1, 2, -1, 4, 100 }, 5, false)]
        [DataRow(1, new int[] { 1 }, 0, true)]
        public void Remove_Element_ReturnedExpectedCount(int elementToRemove, int[] elements, int expectedCount, bool expectedBool)
        {
            //
            //arrange
            //
            CustomList<int> list = new CustomList<int>(elements);
            //
            //act
            //
            var actualBool = list.Remove(elementToRemove);
            //
            //arrange
            //
            Assert.AreEqual(expectedCount, list.Count, message: "Remove method works incorrectly");
            Assert.AreEqual(expectedBool, actualBool, message: "Remove method works incorrectly");
        }

        /// <summary>
        /// Performs a test towards the method RemoveAt that should change property <see cref="CustomList{T}.Count"/>. 
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DataRow(2, new int[] { 1, 2, 3, 4, 5 }, 4, 4)]
        [DataRow(0, new int[] { 1, 2, 3, }, 2, 2)]
        [DataRow(0, new int[] { 1, 5 }, 1, 5)]
        public void RemoveAt_Position_ExpectedCountReturned(int position, int[] elements, int expectedCount, int excpectedResult)
        {
            //
            //arrange
            //
            CustomList<int> list = new CustomList<int>(elements);
            //
            //act
            //
            list.RemoveAt(position);
            var actualResult = list[position];
            //
            //arrange
            //
            Assert.AreEqual(excpectedResult, actualResult, message: "RemoveAt works incorrectly");
            Assert.AreEqual(expectedCount, list.Count, message: "RemoveAt works incorrectly");
        }

        /// <summary>
        /// Performs a test towards the method Insert with wrong parameters that should throw <see cref="ArgumentOutOfRangeException"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DataRow(-2)]
        [DataRow(6)]
        public void Insert_AtWrongPositionValue_ThrowArgumentOutOfRange(int position)
        {
            //
            //arrange
            //
            CustomList<int> list = new CustomList<int>(1, 2, 3, 4, 5);
            var expectedEx = typeof(ArgumentOutOfRangeException);
            //
            //act
            //
            var actEx = Assert.ThrowsException<ArgumentOutOfRangeException>(() => list.Insert(position, 100));
            //
            //assert
            //
            Assert.AreEqual(expectedEx, actEx.GetType(),
                message: "Insert throws ArgumentOutOfRangeException, if position is out of list range ");
        }

        /// <summary>
        /// Performs a test towards the method RemoveAt with wrong position that should throw <see cref="ArgumentOutOfRangeException"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DataRow(-1)]
        [DataRow(3)]
        public void RemoveAt_WrongPosition_ThrowIndexOutOfRangeException(int position)
        {
            //
            //arrange
            //
            CustomList<int> list = new CustomList<int>(1, 0);
            var expectedEx = typeof(ArgumentOutOfRangeException);
            //
            //act
            //
            var actEx = Assert.ThrowsException<ArgumentOutOfRangeException>(() => list.RemoveAt(position));
            //
            //assert  
            //
            Assert.AreEqual(expectedEx, actEx.GetType(),
                message: "RemoveAt throws ArgumentOutOfRangeException in case  position is out of list range  ");
        }

        /// <summary>
        /// Performs a test towards the method Remove with null parameter that should throw <see cref="ArgumentNullException"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Remove_Null_ThrowArgumentNullException()
        {
            //
            //arrange
            //
            CustomList<object> list = new CustomList<object>(1, 2, 3);
            var expectedEx = typeof(ArgumentNullException);
            //
            //act
            //
            var actEx = Assert.ThrowsException<ArgumentNullException>(() => list.Remove(null));
            //
            //assert         
            //
            Assert.AreEqual(expectedEx, actEx.GetType(),
                message: "Remove throws ArgumentNullException in case  item is null ");
        }
    }
}