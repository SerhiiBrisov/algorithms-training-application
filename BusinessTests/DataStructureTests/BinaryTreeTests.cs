﻿using BusinessTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using TrainingApp.Business.DataStructures.BinaryTree;

namespace BusinessTests.DataStructureTests
{
    /// <summary>
    /// Represents the unit tests towards the <see cref="BinaryTree{T}"/> object.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    [TestClass]
    public class BinaryTreeTests
    {
        /// <summary>
        /// Contains <see cref="BinaryTreeTestHelper{int}"/> of type int.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private readonly BinaryTreeTestHelper<int> intTestHelper;

        /// <summary>
        /// Contains <see cref="BinaryTreeTestHelper{ComparableClass}"/> of type <see cref="ComparableClass"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private readonly BinaryTreeTestHelper<ComparableClass> comparableTestHelper;

        /// <summary>
        /// Contains <see cref="BinaryTreeTestHelper{UncomparableClass}"/> of type <see cref="UncomparableClass"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private readonly BinaryTreeTestHelper<UncomparableClass> uncomparableTestHelper;

        /// <summary>
        /// Performs a test towards the method Add and check property <see cref="Count"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DataRow(0)]
        [DataRow(10)]
        [DataRow(100)]
        public void Add_AmountOfElements_CountEqualsAmount(int amount)
        {
            var expected = amount;

            var tree = new BinaryTree<int>();
            for (int i = 0; i < amount; i++)
                tree.Add(intTestHelper.testData[0]);
            var actual = tree.Count;

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Performs a test towards the method Add that should invoke event ElementAdded.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Add_Element_InvokesEventElementAdded()
        {
            var elementWasAdded = false;
            this.comparableTestHelper.tree.ElementAdded += (sender, args) => elementWasAdded = true;

            this.comparableTestHelper.tree.Add(this.comparableTestHelper.testData[0]);

            Assert.IsTrue(elementWasAdded
                , "Add method should invoke ElementAdded event if parameter was added to tree.");
        }

        /// <summary>
        /// Performs a test towards the method Add with parameter null.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Add_Null_ThrowsArgumentNullException()
        {
            ComparableClass nullElement = null;
            var tree = new BinaryTree<ComparableClass>();

            Assert.ThrowsException<ArgumentNullException>(() => tree.Add(nullElement)
            , "Add method should throw ArgumentNullException if parameter is null.");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BinaryTreeTests"/> class.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public BinaryTreeTests()
        {
            this.intTestHelper = new BinaryTreeTestHelper<int>(
                BinaryTreeTestDataClass.IntData[0] as int[],
                Convert.ToInt32(BinaryTreeTestDataClass.IntData[1].ToString()),
                BinaryTreeTestDataClass.IntData[2] as Comparer<int>,
                BinaryTreeTestDataClass.IntData[3] as int[],
                BinaryTreeTestDataClass.IntData[4] as int[],
                BinaryTreeTestDataClass.IntData[5] as int[]
                );

            this.comparableTestHelper = new BinaryTreeTestHelper<ComparableClass>(
                BinaryTreeTestDataClass.ComaparableClassData[0] as ComparableClass[],
                BinaryTreeTestDataClass.ComaparableClassData[1] as ComparableClass,
                BinaryTreeTestDataClass.ComaparableClassData[2] as Comparer<ComparableClass>,
                BinaryTreeTestDataClass.ComaparableClassData[3] as int[],
                BinaryTreeTestDataClass.ComaparableClassData[4] as int[],
                BinaryTreeTestDataClass.ComaparableClassData[5] as int[]
                );

            this.uncomparableTestHelper = new BinaryTreeTestHelper<UncomparableClass>(
                BinaryTreeTestDataClass.UncomaparableClassData[0] as UncomparableClass[],
                BinaryTreeTestDataClass.UncomaparableClassData[1] as UncomparableClass,
                BinaryTreeTestDataClass.UncomaparableClassData[2] as UncomparableClassComparer,
                BinaryTreeTestDataClass.UncomaparableClassData[3] as int[],
                BinaryTreeTestDataClass.UncomaparableClassData[4] as int[],
                BinaryTreeTestDataClass.UncomaparableClassData[5] as int[]
                );
        }

        /// <summary>
        /// Performs a test towards the method Contains for int collection with element that is in tree.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Contains_IntElementThatIsInTree_ReturnsTrue()
        {
            bool actual = this.intTestHelper.tree.Contains(intTestHelper.testData[0]);

            Assert.IsTrue(actual);
        }

        /// <summary>
        /// Performs a test towards the method Contains for comparable collection with element that is in tree.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Contains_ComparableElementThatIsInTree_ReturnsTrue()
        {
            bool actual = this.comparableTestHelper.tree.Contains(comparableTestHelper.testData[0]);

            Assert.IsTrue(actual);
        }

        /// <summary>
        /// Performs a test towards the method Contains for uncomparable collection with element that is in tree.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Contains_UncomparableElementThatIsInTree_ReturnsTrue()
        {
            bool actual = this.uncomparableTestHelper.tree.Contains(uncomparableTestHelper.testData[0]);

            Assert.IsTrue(actual);
        }

        /// <summary>
        /// Performs a test towards the method Contains for int collection with element that is not in tree.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Contains_IntElementThatIsNotInTree_ReturnsFalse()
        {
            bool actual = this.intTestHelper.tree.Contains(intTestHelper.objectNotInTree);

            Assert.IsFalse(actual);
        }

        /// <summary>
        /// Performs a test towards the method Contains for comparable collection with element that is not in tree.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Contains_ComparableElementThatIsNotInTree_ReturnsFalse()
        {
            bool actual = this.comparableTestHelper.tree.Contains(comparableTestHelper.objectNotInTree);

            Assert.IsFalse(actual);
        }

        /// <summary>
        /// Performs a test towards the method Contains for uncomparable collection with element that is not in tree.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Contains_UncomparableElementThatIsNotInTree_ReturnsFalse()
        {
            bool actual = this.uncomparableTestHelper.tree.Contains(uncomparableTestHelper.objectNotInTree);

            Assert.IsFalse(actual);
        }

        /// <summary>
        /// Performs a test towards the method Contains with null element.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Contains_Null_ReturnsFalse()
        {
            ComparableClass nullElement = null;
            var tree = this.comparableTestHelper.CreateTree();

            var actual = tree.Contains(nullElement);

            Assert.IsFalse(actual);
        }

        /// <summary>
        /// Performs a test towards the method Add that should invoke event ElementAdded and check event args.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void EventElementAdded_Invoked_ValueIsAddedItem()
        {
            var expected = this.comparableTestHelper.testData[0];
            ComparableClass actual = this.comparableTestHelper.objectNotInTree;
            this.comparableTestHelper.tree.ElementAdded += (sender, args) => actual = args.Value;

            this.comparableTestHelper.tree.Add(this.comparableTestHelper.testData[0]);

            Assert.AreEqual(expected, actual
                , "Added item should be passed to arguments of event when it was invoked.");
        }

        /// <summary>
        /// Performs a test towards the method Add that should invoke event ElementAdded and check event sender.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void EventElementAdded_Invoked_SenderIsCurrentBinaryTree()
        {
            var expected = this.uncomparableTestHelper.tree;
            BinaryTree<UncomparableClass> actual = null;
            this.uncomparableTestHelper.tree.ElementAdded += (sender, args) => actual = sender as BinaryTree<UncomparableClass>;

            this.uncomparableTestHelper.tree.Add(this.uncomparableTestHelper.testData[0]);

            Assert.AreSame(expected, actual
                , "Current binary tree should be passed as sender in the event.");
        }

        /// <summary>
        /// Performs a test towards the method Remove that should invoke event ElementRemoved and check event args.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void EventElementRemoved_Invoked_ValueIsRemovedItem()
        {
            var expected = this.intTestHelper.testData[0];
            int actual = this.intTestHelper.objectNotInTree;
            this.intTestHelper.tree.ElementRemoved += (sender, args) => actual = args.Value;

            this.intTestHelper.tree.Remove(this.intTestHelper.testData[0]);

            Assert.AreEqual(expected, actual
            , "Removed item should be passed to arguments of event when it was invoked.");
        }

        /// <summary>
        /// Performs a test towards the method Remove that should invoke event ElementRemoved and check event sender.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void EventElementRemoved_Invoked_SenderIsCurrentBinaryTree()
        {
            var expected = this.intTestHelper.tree;
            BinaryTree<int> actual = null;
            this.intTestHelper.tree.ElementAdded += (sender, args) => actual = sender as BinaryTree<int>;
            this.intTestHelper.tree.Add(this.intTestHelper.testData[0]);

            this.intTestHelper.tree.Remove(this.intTestHelper.testData[0]);

            Assert.AreEqual(expected, actual
            , "Current binary tree should be passed as sender in the event.");
        }

        /// <summary>
        /// Performs a test towards the method Remove element that is not in tree.
        /// should not invoke event ElementRemoved
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Remove_ElementThatIsNotInTree_DoesNotInvokeEventElementRemoved()
        {
            var elementWasRemoved = false;
            this.intTestHelper.tree.ElementRemoved += (sender, args) => elementWasRemoved = true;

            this.intTestHelper.tree.Remove(this.intTestHelper.objectNotInTree);

            Assert.IsFalse(elementWasRemoved
            , "Remove method should not invoke event if parameter is not in tree.");
        }

        /// <summary>
        /// Performs a test towards the property Count that should be equal to actual quantity of elements.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DataRow(0)]
        [DataRow(10)]
        [DataRow(100)]
        public void Remove_AmountOfElements_CountEqualsHowManyElementsLeftInTree(int amount)
        {
            var expected = 100 - amount;
            var tree = new BinaryTree<int>();
            for (int i = 0; i < 100; i++)
                tree.Add(this.intTestHelper.testData[0]);

            for (int i = 0; i < amount; i++)
                tree.Remove(this.intTestHelper.testData[0]);
            var actual = tree.Count;

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Performs a test towards the method Traverse with InOrder type for int collection.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Traverse_InOrder_ReturnsIntCollectionInRightOrder()
        {
            int[] expected = this.intTestHelper.inOrderTraverseData;

            var actual = this.intTestHelper.tree.Traverse(TraverseType.InOrder).ToList();

            Assert.IsTrue(actual.SequenceEqual(expected));
        }

        /// <summary>
        /// Performs a test towards the method Traverse with InOrder type for comparable collection.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Traverse_InOrder_ReturnsComparableCollectionInRightOrder()
        {
            ComparableClass[] expected = this.comparableTestHelper.inOrderTraverseData;

            var actual = this.comparableTestHelper.tree.Traverse(TraverseType.InOrder).ToList();

            Assert.IsTrue(actual.SequenceEqual(expected));
        }

        /// <summary>
        /// Performs a test towards the method Traverse with InOrder type for uncomparable collection.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Traverse_InOrder_ReturnsUncomparableCollectionInRightOrder()
        {
            UncomparableClass[] expected = this.uncomparableTestHelper.inOrderTraverseData;

            var actual = this.uncomparableTestHelper.tree.Traverse(TraverseType.InOrder).ToList();

            Assert.IsTrue(actual.SequenceEqual(expected));
        }

        /// <summary>
        /// Performs a test towards the method TreeMax in uncomparable tree.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void TreeMax_TreeHasElements_ReturnsElementWithHighestValue()
        {
            var expected = this.uncomparableTestHelper.testData[2];

            var actual = this.uncomparableTestHelper.tree.TreeMax();

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Performs a test towards the method TreeMin in uncomparable tree.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void TreeMin_TreeHasElements_ReturnsElementWithLowestValue()
        {
            var expected = this.uncomparableTestHelper.testData[3];

            var actual = this.uncomparableTestHelper.tree.TreeMin();

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Performs a test towards the method Traverse with PreOrder type for int collection.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Traverse_PreOrder_ReturnsIntCollectionInRightOrder()
        {
            int[] expected = this.intTestHelper.preOrderTraverseData;

            var actual = this.intTestHelper.tree.Traverse(TraverseType.PreOrder).ToList();

            Assert.IsTrue(actual.SequenceEqual(expected));
        }

        /// <summary>
        /// Performs a test towards the method Traverse with PreOrder type for comparable collection.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Traverse_PreOrder_ReturnsComparableCollectionInRightOrder()
        {
            ComparableClass[] expected = this.comparableTestHelper.preOrderTraverseData;

            var actual = this.comparableTestHelper.tree.Traverse(TraverseType.PreOrder).ToList();

            Assert.IsTrue(actual.SequenceEqual(expected));
        }

        /// <summary>
        /// Performs a test towards the method Traverse with PreOrder type for uncomparable collection
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Traverse_PreOrder_ReturnsUncomparableCollectionInRightOrder()
        {
            UncomparableClass[] expected = this.uncomparableTestHelper.preOrderTraverseData;

            var actual = this.uncomparableTestHelper.tree.Traverse(TraverseType.PreOrder).ToList();

            Assert.IsTrue(actual.SequenceEqual(expected));
        }

        /// <summary>
        /// Performs a test towards the method Traverse with PostOrder type for int collection.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Traverse_PostOrder_ReturnsIntCollectionInRightOrder()
        {
            int[] expected = this.intTestHelper.postOrderTraverseData;

            var actual = this.intTestHelper.tree.Traverse(TraverseType.PostOrder).ToList();

            Assert.IsTrue(actual.SequenceEqual(expected));
        }

        /// <summary>
        /// Performs a test towards the method Traverse with PostOrder type for comparable collection.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Traverse_PostOrder_ReturnsComparableCollectionInRightOrder()
        {
            ComparableClass[] expected = this.comparableTestHelper.postOrderTraverseData;

            var actual = this.comparableTestHelper.tree.Traverse(TraverseType.PostOrder).ToList();

            Assert.IsTrue(actual.SequenceEqual(expected));
        }

        /// <summary>
        /// Performs a test towards the method Traverse with PostOrder type for uncomparable collection.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Traverse_PostOrder_ReturnsUncomparableCollectionInRightOrder()
        {
            UncomparableClass[] expected = this.uncomparableTestHelper.postOrderTraverseData;

            var actual = this.uncomparableTestHelper.tree.Traverse(TraverseType.PostOrder).ToList();

            Assert.IsTrue(actual.SequenceEqual(expected));
        }

        /// <summary>
        /// Performs a test towards the method TreeMax for empty tree.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void TreeMax_TreeIsEmpty_ThrowsInvalidOperationException()
        {
            var emptyTree = new BinaryTree<int>();
            Assert.ThrowsException<InvalidOperationException>(() => emptyTree.TreeMax()
            , "TreeMax method should throw InvalidOperationException if tree contains no elements.");
        }

        /// <summary>
        /// Performs a test towards the method TreeMin for empty tree.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void TreeMin_TreeIsEmpty_ThrowsInvalidOperationException()
        {
            var emptyTree = new BinaryTree<int>();
            Assert.ThrowsException<InvalidOperationException>(() => emptyTree.TreeMin()
            , "TreeMin method should throw InvalidOperationException if tree contains no elements.");
        }

        /// <summary>
        /// Performs a test towards the method Remove with an element that is in tree.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Remove_ElementThatIsInTree_ReturnsTrue()
        {
            var actual = this.intTestHelper.tree.Remove(this.intTestHelper.testData[3]);

            Assert.IsTrue(actual);
        }

        /// <summary>
        /// Performs a test towards the method Remove with an element that is not in tree.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Remove_UnexistingElement_ReturnsFalse()
        {
            var actual = this.intTestHelper.tree.Remove(this.intTestHelper.objectNotInTree);

            Assert.IsFalse(actual);
        }

        /// <summary>
        /// Performs a test towards the method Remove with null element.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [TestMethod]
        public void Remove_Null_ReturnsFalse()
        {
            ComparableClass nullElement = null;
            var tree = new BinaryTree<ComparableClass>();

            var actual = tree.Remove(nullElement);

            Assert.IsFalse(actual);
        }
    }
}