﻿namespace BusinessTests.Helpers
{
    /// <summary>
    /// Uncomparable class for testing algorithms and data structures in this project.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class UncomparableClass
    {
        /// <summary>
        /// Override of <see cref="Object.ToString()"/> method.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public override string ToString() => $"Value - {this.Value}";

        /// <summary>
        /// Keeps value stored in this class.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>Value of this instance.</value>
        public int Value { get; set; }
    }
}