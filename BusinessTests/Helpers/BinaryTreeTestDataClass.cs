﻿using System.Collections;
using System.Collections.Generic;

namespace BusinessTests.Helpers
{
    /// <summary>
    /// Class includes data for <see cref="BinaryTreeTests"></see> object.
    /// </summary
    /// <owner>Serhii Brysov</owner>
    internal static class BinaryTreeTestDataClass
    {
        /// <summary>
        /// Create instances of test data.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        static BinaryTreeTestDataClass()
        {
            IntData = new ArrayList
            {
                new int[] { 100, 60, 400, 10, 90 },
                20,
                Comparer<int>.Default,
                new int[] { 0, 1, 3, 4, 2 },
                new int[] { 3, 1, 4, 0, 2 },
                new int[] { 3, 4, 1, 2, 0 }
            };

            ComaparableClassData = new ArrayList
            {
                new ComparableClass[]
                {
                    new ComparableClass{ Value = 100 },
                    new ComparableClass{ Value = 60 },
                    new ComparableClass{ Value = 400 },
                    new ComparableClass{ Value = 10 },
                    new ComparableClass{ Value = 90 }
                },
                new ComparableClass { Value = 20 },
                Comparer<ComparableClass>.Default,
                new int[] { 0, 1, 3, 4, 2 },
                new int[] { 3, 1, 4, 0, 2 },
                new int[] { 3, 4, 1, 2, 0 }
            };

            UncomaparableClassData = new ArrayList
            {
                new UncomparableClass[]
                 {
                     new UncomparableClass{ Value = 100 },
                     new UncomparableClass{ Value = 60 },
                     new UncomparableClass{ Value = 400 },
                     new UncomparableClass{ Value = 10 },
                     new UncomparableClass{ Value = 90 }
                 },
                new UncomparableClass { Value = 20 },
                new UncomparableClassComparer(),
                new int[] { 0, 1, 3, 4, 2 },
                new int[] { 3, 1, 4, 0, 2 },
                new int[] { 3, 4, 1, 2, 0 }
            };
        }

        /// <summary>
        /// Represents data for tests towards <see cref="BinaryTree{ComparableClass}"></see> object.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>List of ComparableClass objects.</value>
        public static ArrayList ComaparableClassData { get; set; }

        /// <summary>
        /// Represents data for tests towards <see cref="BinaryTree{int}"></see> object.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>List of int data.</value>
        public static ArrayList IntData { get; set; }

        /// <summary>
        /// Represents data for tests towards <see cref="BinaryTree{UncomparableClass}"></see> object.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>List of UncomparableClass objects.</value>
        public static ArrayList UncomaparableClassData { get; set; }
    }
}