﻿using System;

namespace BusinessTests.Helpers
{
    /// <summary>
    /// Class implementing <see cref="IComparable"/> interface.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class ComparableClass : IComparable<ComparableClass>
    {
        /// <summary>
        /// Realization of <see cref="IComparable.CompareTo(object)"/> method.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public int CompareTo(ComparableClass other) => this.Value.CompareTo(other.Value);

        /// <summary>
        /// Override of <see cref="Object.ToString()"/> method.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public override string ToString() => $"Value - {this.Value}";

        /// <summary>
        /// Keeps value stored in this class.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>Value of this instance.</value>
        public int Value { get; set; }
    }
}