﻿using System.Collections.Generic;
using System.Linq;
using TrainingApp.Business.DataStructures.BinaryTree;

namespace BusinessTests.Helpers
{
    /// <summary>
    /// Class that provides helping data for <see cref="BinaryTreeTests"/>.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class BinaryTreeTestHelper<T>
    {
        /// <summary>
        /// The test data for tests of <see cref="BinaryTree{T}"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public readonly T[] testData;

        /// <summary>
        /// The object not in tree.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public readonly T objectNotInTree;

        /// <summary>
        /// The comparer.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public readonly IComparer<T> comparer;

        /// <summary>
        /// The pre order traverse data.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public readonly T[] preOrderTraverseData;

        /// <summary>
        /// The in order traverse data.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public readonly T[] inOrderTraverseData;

        /// <summary>
        /// The post order traverse data.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public readonly T[] postOrderTraverseData;

        /// <summary>
        /// The binary tree.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public BinaryTree<T> tree;

        /// <summary>
        /// Initializes a new instance of the <see cref="BinaryTreeTestHelper{T}"/> class.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="testData">The test data.</param>
        /// <param name="objectNotInTree">The object not in tree.</param>
        /// <param name="comparer">The comparer.</param>
        /// <param name="preOrderTraverseIndexes">The pre order traverse indexes.</param>
        /// <param name="inOrderTraverseIndexes">The in order traverse indexes.</param>
        /// <param name="postOrderTraverseIndexes">The post order traverse indexes.</param>
        public BinaryTreeTestHelper(T[] testData, T objectNotInTree, IComparer<T> comparer,
            int[] preOrderTraverseIndexes, int[] inOrderTraverseIndexes, int[] postOrderTraverseIndexes)
        {
            this.testData = testData;
            this.objectNotInTree = objectNotInTree;
            this.comparer = comparer;
            this.preOrderTraverseData = preOrderTraverseIndexes.Select(i => this.testData[i]).ToArray();
            this.inOrderTraverseData = inOrderTraverseIndexes.Select(i => this.testData[i]).ToArray();
            this.postOrderTraverseData = postOrderTraverseIndexes.Select(i => this.testData[i]).ToArray();
            this.tree = this.CreateTree();
            foreach (var number in testData)
                this.tree.Add(number);
        }

        /// <summary>
        /// Creates the tree.
        /// </summary>
        /// <returns>New empty tree</returns>
        public BinaryTree<T> CreateTree()
        {
            if (this.comparer == Comparer<T>.Default)
                return new BinaryTree<T>();
            else
                return new BinaryTree<T>(this.comparer);
        }
    }
}