﻿using BusinessTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using TrainingApp.Business.Models;
using TrainingApp.Business.Sortings;

namespace BusinessTests.SortingAlgorithmsTests
{
    /// <summary>
    /// Represents the unit tests towards the <see cref="BubbleSort{T}"/>, <see cref="InsertionSort{T}"/>, <see cref="ShakerSort{T}"/>  objects.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    [TestClass]
    public class SortingTest
    {
        /// <summary>
        /// Performs a test towards the method Sort of type <see cref="BubbleSort{int}"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DataRow(0)]
        [DataRow(8, 19, 1723, 717, 1, 0, 18)]
        [DataRow(-1, -2, -3, -4, -5, -6, -7)]
        [DataRow(0, 0, 0, 0, 0)]
        public void BubbleSort_IntList_CollectionIsSorted(params int[] elements)
        {
            //
            //arrange
            //
            BubbleSort<int> bubbleSort = new BubbleSort<int>();
            ComparableModel<int> actualModel = new ComparableModel<int>(elements);
            List<int> expected = new List<int>(elements.Length);
            foreach (var item in elements)
            {
                expected.Add(item);
            }
            expected.Sort();
            //
            //act
            //
            bubbleSort.Sort(actualModel, Comparer<int>.Default, 0);
            //
            //assert
            //
            Assert.IsTrue(expected.SequenceEqual(actualModel.SortingCollection));
        }

        /// <summary>
        /// Performs a test towards the method Sort of type <see cref="BubbleSort{UncomparableClass}"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DynamicData(nameof(SortTestUncomparableDataProperty), DynamicDataSourceType.Property)]
        public void BubbleSort_UncomparableClassList_CollectionIsSorted(UncomparableClass[] elements)
        {
            //
            //arrange
            //
            BubbleSort<UncomparableClass> shakerSort = new BubbleSort<UncomparableClass>();
            ComparableModel<UncomparableClass> actualModel = new ComparableModel<UncomparableClass>(elements);
            var expected = new List<UncomparableClass>(elements.Length);
            foreach (var item in elements)
            {
                expected.Add(item);
            }
            expected.Sort(new UncomparableClassComparer());
            //
            //act
            //
            shakerSort.Sort(actualModel, new UncomparableClassComparer(), 0);
            //
            //assert
            //
            Assert.IsTrue(expected.SequenceEqual(actualModel.SortingCollection));
        }

        /// <summary>
        /// Performs a test towards the method Sort of type <see cref="InsertionSort{UncomparableClass}"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DynamicData(nameof(SortTestUncomparableDataProperty), DynamicDataSourceType.Property)]
        public void InsertionSort_UncomparableClassList_CollectionIsSorted(UncomparableClass[] elements)
        {
            //
            //arrange
            //
            InsertionSort<UncomparableClass> shakerSort = new InsertionSort<UncomparableClass>();
            ComparableModel<UncomparableClass> actualModel = new ComparableModel<UncomparableClass>(elements);
            var expected = new List<UncomparableClass>(elements.Length);
            foreach (var item in elements)
            {
                expected.Add(item);
            }
            expected.Sort(new UncomparableClassComparer());
            //
            //act
            //
            shakerSort.Sort(actualModel, new UncomparableClassComparer(), 0);//
            //assert
            //
            Assert.IsTrue(expected.SequenceEqual(actualModel.SortingCollection));
        }

        /// <summary>
        /// Performs a test towards the method Sort of type <see cref="InsertionSort{int}"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DataRow(0)]
        [DataRow(8, 19, 1723, 717, 1, 0, 18)]
        [DataRow(-1, -2, -3, -4, -5, -6, -7)]
        [DataRow(0, 0, 0, 0, 0)]
        public void InsertionSort_IntList_CollectionIsSorted(params int[] elements)
        {
            //
            //arrange
            //
            InsertionSort<int> insertionSort = new InsertionSort<int>();
            ComparableModel<int> actualModel = new ComparableModel<int>(elements);
            List<int> expected = new List<int>(elements.Length);
            foreach (var item in elements)
            {
                expected.Add(item);
            }
            expected.Sort();
            //
            //act
            //
            insertionSort.Sort(actualModel, Comparer<int>.Default, 0);
            //
            //assert
            //
            Assert.IsTrue(expected.SequenceEqual(actualModel.SortingCollection));
        }

        /// <summary>
        /// Performs a test towards the method Sort of type <see cref="ShakerSort{int}"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DataRow(0)]
        [DataRow(8, 19, 1723, 717, 1, 0, 18)]
        [DataRow(-1, -2, -3, -4, -5, -6, -7)]
        [DataRow(0, 0, 0, 0, 0)]
        public void ShakerSort_IntList_CollectionIsSorted(params int[] elements)
        {
            //
            //arrange
            //
            ShakerSort<int> shakerSort = new ShakerSort<int>();
            ComparableModel<int> actualModel = new ComparableModel<int>(elements);
            List<int> expected = new List<int>(elements.Length);
            foreach (var item in elements)
            {
                expected.Add(item);
            }
            expected.Sort();
            //
            //act
            //
            shakerSort.Sort(actualModel, Comparer<int>.Default, 0);
            //
            //assert
            //
            Assert.IsTrue(expected.SequenceEqual(actualModel.SortingCollection));
        }

        /// <summary>
        /// Performs a test towards the method Sort of type <see cref="ShakerSort{UncomparableClass}"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DynamicData(nameof(SortTestUncomparableDataProperty), DynamicDataSourceType.Property)]
        public void ShakerSort_UncomparableClassList_CollectionIsSorted(UncomparableClass[] elements)
        {
            //
            //arrange
            //
            ShakerSort<UncomparableClass> shakerSort = new ShakerSort<UncomparableClass>();
            ComparableModel<UncomparableClass> actualModel = new ComparableModel<UncomparableClass>(elements);
            List<UncomparableClass> expected = new List<UncomparableClass>(elements.Length);
            foreach (var item in elements)
            {
                expected.Add(item);
            }
            expected.Sort(new UncomparableClassComparer());
            //
            //act
            //
            shakerSort.Sort(actualModel, new UncomparableClassComparer(), 0);
            //
            //assert
            //
            Assert.IsTrue(expected.SequenceEqual(actualModel.SortingCollection));
        }        

        /// <summary>
        /// Performs a test towards the method Sort of type <see cref="ShakerSort{ComparableClass}"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [DataTestMethod]
        [DynamicData(nameof(SortTestComparableDataProperty), DynamicDataSourceType.Property)]
        public void ShakerSort_ComparableClassList_CollectionIsSorted(ComparableClass[] elements)
        {
            //
            //arrange
            //
            ShakerSort<ComparableClass> shakerSort = new ShakerSort<ComparableClass>();
            ComparableModel<ComparableClass> actualModel = new ComparableModel<ComparableClass>(elements);
            List<ComparableClass> expected = new List<ComparableClass>(elements.Length);
            foreach (var item in elements)
            {
                expected.Add(item);
            }
            expected.Sort();
            //
            //act
            //
            shakerSort.Sort(actualModel, Comparer<ComparableClass>.Default, 0);
            //
            //assert
            //
            Assert.IsTrue(expected.SequenceEqual(actualModel.SortingCollection));
        }

        /// <summary>
        /// Gets dynamic data for unit tests in this test class.
        /// </summary>       
        /// <values>
        /// Enumerable sequence of <see cref="UncomparableClass"/> objects.
        /// </values>
        /// <owner>Serhii Brysov</owner
        private static IEnumerable<object[]> SortTestUncomparableDataProperty
        {
            get
            {
                yield return new object[] { new UncomparableClass[]
                 {
                     new UncomparableClass{ Value = 100 },
                     new UncomparableClass{ Value = 60 },
                     new UncomparableClass{ Value = 400 },
                     new UncomparableClass{ Value = 10 },
                     new UncomparableClass{ Value = 90 }
                 } };
                yield return new object[] { new UncomparableClass[]
                 {
                     new UncomparableClass{ Value = 0 },
                     new UncomparableClass{ Value = 1 },
                     new UncomparableClass{ Value = -1 },
                 } };
                yield return new object[] { new UncomparableClass[]
                 {
                     new UncomparableClass{ Value = 0 }
                 } };
            }
        }

        /// <summary>
        /// Gets dynamic data for unit tests in this test class.
        /// </summary>       
        /// <values>
        /// Enumerable sequence of <see cref="ComparableClass"/> objects.
        /// </values>
        /// <owner>Serhii Brysov</owner
        private static IEnumerable<object[]> SortTestComparableDataProperty
        {
            get
            {
                yield return new object[] { new ComparableClass[]
                 {
                     new ComparableClass{ Value = 100 },
                     new ComparableClass{ Value = 60 },
                     new ComparableClass{ Value = 400 },
                     new ComparableClass{ Value = 10 },
                     new ComparableClass{ Value = 90 }
                 } };
                yield return new object[] { new ComparableClass[]
                 {
                     new ComparableClass{ Value = 0 },
                     new ComparableClass{ Value = 1 },
                     new ComparableClass{ Value = -1 },
                 } };
                yield return new object[] { new ComparableClass[]
                 {
                     new ComparableClass{ Value = 0 }
                 } };
            }
        }
    }
}